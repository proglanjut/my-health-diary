import 'package:flutter/material.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/myPlansEnv/ChecklistItems.dart';
import 'package:myhealthdiary/myPlansEnv/DailyChecklistSettings.dart';
import 'package:intl/intl.dart';

import '../appTheme.dart';

class AddChecklist extends StatefulWidget {
  String get title => "Add Checklist";

  @override
  _AddChecklistPage createState() => _AddChecklistPage();
}

class _AddChecklistPage extends State<AddChecklist> {
  ScrollController controller;
  TabController tabController;
  bool isChanged = false;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  void backToChecklist() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => DailyChecklist()),
    );
  }

  Future<void> _changeDataConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Are you sure you want to change your checklist(s) plans?\n'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                backToChecklist();
              },
            ),
            FlatButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: AppTheme.blueTheme,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _returnConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Text('Confirmation'),
          content: Container(
            child: Text('Are you sure you want to exit?\n'),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                backToChecklist();
              },
            ),
            FlatButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget getTitle(data) {
    return Text(
      '${data['title']}',
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
    );
  }

  Widget getDesc(data) {
    return Text(
      '${data['description']}',
    );
  }

  Widget getSwitch(data) {
    return Ink(
      decoration: const ShapeDecoration(
        color: AppTheme.blueTheme,
        shape: CircleBorder(),
      ),
      child: IconButton(
        onPressed: () {
          bool alreadyInPlan = false;
          DummyUser.loggedUser['checklist']['onList'].forEach((checklistInPlan){
            if(checklistInPlan['id'] == data['id']) alreadyInPlan = true;
          });
          if(!alreadyInPlan){
            isChanged = true;
            data['used']++;
            DateTime date = DateTime.now();
  //        DummyUser.loggedUser['checklist']['onList'].add(data);
  //        DummyUser.loggedUser['checklist']['lastChange'] = date.toString();
            DummyUser.userData.forEach((user) {
              if (user['id'] == DummyUser.loggedUser['id']) {
                user['checklist']['onList'].add(data);
                user['checklist']['lastChange'] = date.toString();
              }
            });
            scaffoldKey.currentState.showSnackBar(
                SnackBar(content: Text("\"${data['title']}\" was added to your plans!")));
          }else{
            scaffoldKey.currentState.showSnackBar(
              SnackBar(
                content: Text("\"${data['title']}\" already in your plans!"),
                backgroundColor: Colors.red,
              )
            );
          }
        },
        color: Colors.white,
        icon: Icon(Icons.add),
      )
    );
  }

  Widget getCompletion(data) {
    int completion = data['completion'], used = data['used'];
    if (used == 0) {
      return Text(
        "Completion: Never used",
        style: TextStyle(
          fontSize: 12.0,
        ),
      );
    } else {
      return Text(
        "Completion: $completion of $used",
        style: TextStyle(
          fontSize: 12.0,
        ),
      );
    }
  }

  Widget getCategory(data) {
    IconData whatIcon;
    String whatCategory;
    switch (data['category']) {
      case "food":
        whatIcon = Icons.local_dining;
        whatCategory = " Food and Drink";
        break;
      case "motion":
        whatIcon = Icons.accessibility_new;
        whatCategory = " Body Motions";
        break;
      case "sleep":
        whatIcon = Icons.local_hotel;
        whatCategory = " Sleep";
        break;
      case "habits":
        whatIcon = Icons.airline_seat_recline_extra;
        whatCategory = " Healthy Habits";
        break;
    }
    return Row(
      children: <Widget>[
        Icon(
          whatIcon,
          size: 11,
          color: AppTheme.blueTheme,
        ),
        Text(
          whatCategory,
          style: TextStyle(color: AppTheme.blueTheme, fontSize: 11),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var food = ChecklistItems.foodChecklist,
        motion = ChecklistItems.motionChecklist,
        sleep = ChecklistItems.sleepChecklist,
        habit = ChecklistItems.habitChecklist;

    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    // TODO: implement build
    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },
      child: DefaultTabController(
        length: 4,
        child: Scaffold(
            key: scaffoldKey,
            resizeToAvoidBottomInset: false,
            backgroundColor: AppTheme.notWhite,
            appBar: AppBar(
              title: toolbar,
              backgroundColor: AppTheme.blueTheme,
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    isChanged ? _returnConfirmation() : backToChecklist();
                  }),
              bottom: TabBar(
                indicatorColor: Colors.lightBlueAccent,
                labelColor: Colors.lightBlueAccent,
                unselectedLabelColor: AppTheme.notWhite,
                tabs: <Widget>[
                  Tab(
                    icon: Icon(Icons.local_dining),
                    text: "Food",
                  ),
                  Tab(
                    icon: Icon(Icons.accessibility_new),
                    text: "Body Motion",
                  ),
                  Tab(
                    icon: Icon(Icons.local_hotel),
                    text: "Sleep",
                  ),
                  Tab(
                    icon: Icon(Icons.airline_seat_recline_extra),
                    text: "Habits",
                  ),
                ],
              ),
            ),
            body: TabBarView(
              children: <Widget>[
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  controller: controller,
                  padding: EdgeInsets.all(10.0),
                  itemCount: food.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      getCategory(food[index]),
                                      getTitle(food[index]),
                                      getDesc(food[index]),
                                    ],
                                  )),
                                  getSwitch(food[index])
                                ],
                              ),
                              Divider(
                                color: AppTheme.grey,
                              ),
                              getCompletion(food[index]),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  controller: controller,
                  padding: EdgeInsets.all(10.0),
                  itemCount: motion.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      getCategory(motion[index]),
                                      getTitle(motion[index]),
                                      getDesc(motion[index]),
                                    ],
                                  )),
                                  getSwitch(motion[index])
                                ],
                              ),
                              Divider(
                                color: AppTheme.grey,
                              ),
                              getCompletion(motion[index]),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  controller: controller,
                  padding: EdgeInsets.all(10.0),
                  itemCount: sleep.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      getCategory(sleep[index]),
                                      getTitle(sleep[index]),
                                      getDesc(sleep[index]),
                                    ],
                                  )),
                                  getSwitch(sleep[index])
                                ],
                              ),
                              Divider(
                                color: AppTheme.grey,
                              ),
                              getCompletion(sleep[index]),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  controller: controller,
                  padding: EdgeInsets.all(10.0),
                  itemCount: habit.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      getCategory(habit[index]),
                                      getTitle(habit[index]),
                                      getDesc(habit[index]),
                                    ],
                                  )),
                                  getSwitch(habit[index])
                                ],
                              ),
                              Divider(
                                color: AppTheme.grey,
                              ),
                              getCompletion(habit[index]),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ],
            )),
      ),
    );
  }
}
