import 'package:flutter/material.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:myhealthdiary/main/MyPlans.dart';
import 'package:intl/intl.dart';

import 'package:myhealthdiary/appTheme.dart';
import 'package:numberpicker/numberpicker.dart';

class BodyWeight extends StatefulWidget{
  String get title => "Change Body Weight Plans";

  @override
  State<StatefulWidget> createState() {
    return _BodyWeightPage();
  }

}

class _BodyWeightPage extends State<BodyWeight>{
  static Map<String, dynamic> userInfo = DummyUser.loggedUser;
  List<Map<String, dynamic>> userDatabase = DummyUser.userData;

  ScrollController controller;
  int bodyWeightVal = userInfo['plans']['bodyWeight']['target'];
  double averageVal = userInfo['plans']['bodyWeight']['average'];
  bool isChanged = false;
  DateFormat dateFormat = DateFormat('yMMMMd'); //Output example: May 1, 2020

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  void backToMyPlans (){
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => HomePage(3)),
          (Route<dynamic> route) => false,
    );
  }

  Future<void> _changeDataConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure you want to change your plans?\n'),
                Text('Warning:\nYour current progression will be lost and all data about Body Weight will be reseted!',
                  style: TextStyle(
                      color: Colors.red,
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                //If data's changed, save first
                if(isChanged){
                  userInfo['plans']['bodyWeight']['lastChange'] = DateTime.now().toString();
                  userInfo['plans']['bodyWeight']['target'] = bodyWeightVal;
                  userInfo['plans']['bodyWeight']['average'] = averageVal;
                  userDatabase.forEach((user){
                    if(user['id'] == userInfo['id']){
                      user['plans']['bodyWeight']['lastChange'] = DateTime.now().toString();
                      user['plans']['bodyWeight']['target'] = bodyWeightVal;
                      user['plans']['bodyWeight']['average'] = averageVal;
                    }
                  });
                }
                backToMyPlans();
              },
            ),
            FlatButton(
              child: Text('No',
                style: TextStyle(
                  color: AppTheme.blueTheme,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _returnConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure you want to exit?\n'),
                Text('Warning:\nYou will lost your current change!',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                backToMyPlans();
              },
            ),
            FlatButton(
              child: Text('No',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    DateTime date = DateTime.parse(userInfo['plans']['bodyWeight']['lastChange']);
    String lastChanged = dateFormat.format(date);

    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return WillPopScope(
      onWillPop: () {
        isChanged ? _returnConfirmation() : moveToLastScreen();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: AppTheme.blueTheme,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                isChanged ? _returnConfirmation() : moveToLastScreen();
              }
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                _changeDataConfirmation();
              }
            )
          ],
        ),
        body: ListView(
            shrinkWrap: true,
            controller: controller,
            padding: EdgeInsets.all(10.0),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Your Current Body Weight Plans Information',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Last Changed',
                              style: TextStyle(fontSize: 14.0),
                            ),
                            Text(
                              lastChanged,
                              style: TextStyle(fontSize: 14.0),
                            )
                          ],
                        ),
                        Divider(
                          color: AppTheme.grey,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Initial Body Weight',
                              style: TextStyle(fontSize: 14.0),
                            ),
                            Text(
                              '60 kg',
                              style: TextStyle(fontSize: 14.0),
                            )
                          ],
                        ),
                        Divider(
                          color: AppTheme.grey,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Progression To Target',
                              style: TextStyle(fontSize: 14.0),
                            ),
                            Text(
                              '30%',
                              style: TextStyle(fontSize: 14.0),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Divider(
                        color: AppTheme.grey,
                      ),
                    ),
                    Text(
                      'Scroll the number and put in the middle to change your target',
                      style: TextStyle(
                        color: AppTheme.grey,
                        fontSize: 12
                      ),
                    ),
                    Expanded(
                      child: Divider(
                        color: AppTheme.grey,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Change Body Weight Target',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        Text("Range 30 to 150 kg",
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            NumberPicker.integer(
                              initialValue: bodyWeightVal,
                              minValue: 030,
                              maxValue: 150,
                              onChanged: (newValue) {
                                isChanged = true;
                                setState(() => bodyWeightVal = newValue);
                              },
                            ),
                            Text('kg',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: AppTheme.blueTheme
                                )
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        )
                      ],
                    ),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Change Body Weight Gain/Loss Average Target',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        Text("Range -15 to 15 with 1 decimal place in second column",
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            NumberPicker.decimal(
                              initialValue: averageVal,
                              minValue: -15,
                              maxValue: 15,
                              onChanged: (newValue) {
                                isChanged = true;
                                setState(() => averageVal = newValue);
                              },
                            ),
                            Text('kg/day',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: AppTheme.blueTheme
                                )
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ]
        ),

      ),
    );
  }

}