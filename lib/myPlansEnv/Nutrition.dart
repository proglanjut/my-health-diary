import 'package:flutter/material.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:myhealthdiary/main/MyPlans.dart';
import 'package:intl/intl.dart';

import 'package:myhealthdiary/appTheme.dart';
import 'package:numberpicker/numberpicker.dart';

class Nutrition extends StatefulWidget{
  String get title => "Change Nutrition Plans";

  @override
  State<StatefulWidget> createState() {
    return _NutritionPage();
  }

}

class _NutritionPage extends State<Nutrition>{
  static Map<String, dynamic> userInfo = DummyUser.loggedUser;
  List<Map<String, dynamic>> userDatabase = DummyUser.userData;

  ScrollController controller;
  int caloriesVal = userInfo['plans']['nutrition']['calories'],
      carbohydrateVal = userInfo['plans']['nutrition']['carbo'],
      proteinVal = userInfo['plans']['nutrition']['protein'],
      fatVal = userInfo['plans']['nutrition']['fat'];
  bool isChanged = false;
  DateFormat dateFormat = DateFormat('yMMMMd'); //Output example: May 1, 2020

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  void backToMyPlans (){
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => HomePage(3)),
          (Route<dynamic> route) => false,
    );
  }

  Future<void> _changeDataConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure you want to change your plans?\n'),
                Text('Warning:\nYour current progression will be lost and all data about Calories will be reseted!',
                  style: TextStyle(
                      color: Colors.red,
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                if(isChanged){
                  userInfo['plans']['nutrition']['lastChange'] = DateTime.now().toString();
                  userInfo['plans']['nutrition']['calories'] = caloriesVal;
                  userInfo['plans']['nutrition']['carbo'] = carbohydrateVal;
                  userInfo['plans']['nutrition']['fat'] = fatVal;
                  userInfo['plans']['nutrition']['protein'] = proteinVal;
                  userDatabase.forEach((user){
                    if(user['id'] == userInfo['id']){
                      user['plans']['nutrition']['lastChange'] = DateTime.now().toString();
                      user['plans']['nutrition']['calories'] = caloriesVal;
                      user['plans']['nutrition']['carbo'] = carbohydrateVal;
                      user['plans']['nutrition']['fat'] = fatVal;
                      user['plans']['nutrition']['protein'] = proteinVal;
                    }
                  });
                }
                backToMyPlans();
              },
            ),
            FlatButton(
              child: Text('No',
                style: TextStyle(
                  color: AppTheme.blueTheme,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _returnConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure you want to exit?\n'),
                Text('Warning:\nYou will lost your current change!',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                backToMyPlans();
              },
            ),
            FlatButton(
              child: Text('No',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _errorDialog() async {

    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),
          title: Text('Error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Total percentage of Carbohydrate, Protein, and Fat must 100%'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay. I get it',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    DateTime date = DateTime.parse(userInfo['plans']['nutrition']['lastChange']);
    String lastChanged = dateFormat.format(date);

    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return WillPopScope(
      onWillPop: () {
        isChanged ? _returnConfirmation() : moveToLastScreen();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: AppTheme.blueTheme,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                isChanged ? _returnConfirmation() : moveToLastScreen();
              }
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                ((carbohydrateVal + proteinVal + fatVal) == 100) ? _changeDataConfirmation() : _errorDialog();
              }
            )
          ],
        ),
        body: ListView(
            shrinkWrap: true,
            controller: controller,
            padding: EdgeInsets.all(10.0),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Your Current Calories and Food Plans Information',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Last Changed',
                              style: TextStyle(fontSize: 14.0),
                            ),
                            Text(
                              lastChanged,
                              style: TextStyle(fontSize: 14.0),
                            )
                          ],
                        ),
                        Divider(
                          color: AppTheme.grey,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Total calories target passed',
                              style: TextStyle(fontSize: 14.0),
                            ),
                            Text(
                              '4 times',
                              style: TextStyle(fontSize: 14.0),
                            )
                          ],
                        ),
                        Divider(
                          color: AppTheme.grey,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'Total nutrition percentage target passed\n(error toleration 5%)',
                                style: TextStyle(fontSize: 14.0),
                              ),
                            ),
                            Text(
                              '1 times',
                              style: TextStyle(fontSize: 14.0),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Divider(
                        color: AppTheme.grey,
                      ),
                    ),
                    Text(
                      'Scroll the number and put in the middle to change your target',
                      style: TextStyle(
                        color: AppTheme.grey,
                        fontSize: 12
                      ),
                    ),
                    Expanded(
                      child: Divider(
                        color: AppTheme.grey,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Change Calories Target',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        Text("Range 100 to 5000 kcal with 100 steps",
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            NumberPicker.integer(
                              initialValue: caloriesVal,
                              step: 100,
                              minValue: 0100,
                              maxValue: 5000,
                              onChanged: (newValue) {
                                isChanged = true;
                                setState(() => caloriesVal = newValue);
                              },
                            ),
                            Text('kcal',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: AppTheme.blueTheme
                                )
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        )
                      ],
                    ),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Change Nutrition Percentage Target',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        Text("All percentage sums must be 100%",
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: NumberPicker.integer(
                                          initialValue: carbohydrateVal,
                                          minValue: 0,
                                          maxValue: 100,
                                          onChanged: (newValue) {
                                            isChanged = true;
                                            setState(() => carbohydrateVal = newValue);
                                          },
                                        ),
                                      ),
                                      Text('+',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: AppTheme.blueTheme
                                          )
                                      ),
                                    ],
                                  ),
                                  Text("Carbohydrate")
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: NumberPicker.integer(
                                          initialValue: proteinVal,
                                          minValue: 0,
                                          maxValue: 100,
                                          onChanged: (newValue) {
                                            isChanged = true;
                                            setState(() => proteinVal = newValue);
                                          },
                                        ),
                                      ),
                                      Text('+',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: AppTheme.blueTheme
                                          )
                                      ),
                                    ],
                                  ),
                                  Text("Protein")
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: NumberPicker.integer(
                                          initialValue: fatVal,
                                          minValue: 0,
                                          maxValue: 100,
                                          onChanged: (newValue) {
                                            isChanged = true;
                                            setState(() => fatVal = newValue);
                                          },
                                        ),
                                      ),
                                      Text('%',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: AppTheme.blueTheme
                                          )
                                      ),
                                    ],
                                  ),
                                  Text("Fat")
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ]
        ),

      ),
    );
  }

}