import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:myhealthdiary/myPlansEnv/AddChecklist.dart';
import 'package:myhealthdiary/myPlansEnv/ChecklistItems.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../appTheme.dart';

class DailyChecklist extends StatefulWidget {
  String get title => "Edit Daily Checklist";

  @override
  _DailyChecklistPage createState() => _DailyChecklistPage();
}

class _DailyChecklistPage extends State<DailyChecklist> {
  ScrollController controller;
  bool isChanged = false;
  DateFormat format = DateFormat('yMMMMd'); //Output example: May 1, 2020

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  void goToAddChecklist() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => AddChecklist()),
    );
  }

  void backToMyPlans() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => HomePage(3)),
      (Route<dynamic> route) => false,
    );
  }

  Future<void> _changeDataConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Are you sure you want to change your checklist(s) plans?\n'),
                Text(
                  'Warning:\nYour today\'s checklist progression be reseted!',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                backToMyPlans();
              },
            ),
            FlatButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: AppTheme.blueTheme,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _returnConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure you want to exit?\n'),
                Text(
                  'Warning:\nYou will lost your current change!',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                backToMyPlans();
              },
            ),
            FlatButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget getTitle(data) {
    return Text(
      '${data['title']}',
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
    );
  }

  Widget getDesc(data) {
    return Text(
      '${data['description']}',
    );
  }

  Widget getCategory(data) {
    IconData whatIcon;
    String whatCategory;
    switch (data['category']) {
      case "food":
        whatIcon = Icons.local_dining;
        whatCategory = " Food and Drink";
        break;
      case "motion":
        whatIcon = Icons.accessibility_new;
        whatCategory = " Body Motions";
        break;
      case "sleep":
        whatIcon = Icons.local_hotel;
        whatCategory = " Sleep";
        break;
      case "habits":
        whatIcon = Icons.airline_seat_recline_extra;
        whatCategory = " Healthy Habits";
        break;
    }
    return Row(
      children: <Widget>[
        Icon(
          whatIcon,
          size: 11,
          color: AppTheme.blueTheme,
        ),
        Text(
          whatCategory,
          style: TextStyle(color: AppTheme.blueTheme, fontSize: 11),
        )
      ],
    );
  }

  Widget showProgressBar(data) {
    int complete = data['completion'], used = data['used'];
    double percent = (complete / used);
    return CircularPercentIndicator(
      radius: 50.0,
      lineWidth: 4.0,
      percent: percent,
      center: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('$complete',
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
          Text('\/$used',
              style: TextStyle(
                fontSize: 10.0,
              )),
        ],
      ),
      progressColor: (percent <= 0.5)
          ? Colors.red
          : (percent == 1) ? Colors.green : AppTheme.blue,
    );
  }

  Widget willWeShowTheList(int count, Map<String, dynamic> selectedChecklists){
    if(count == 0){
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(":'(",
                style: TextStyle(
                    color: AppTheme.blueTheme,
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold
                )
            ),
            SizedBox(height: 20,),
            Text("You didn't have any checklist items. Set them now!",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: selectedChecklists['onList'].length,
        physics: ClampingScrollPhysics(),
        itemBuilder: (context, index) {
          var item = selectedChecklists['onList'][index];
          return Dismissible(
            key: Key(item['title']),
            onDismissed: (direction) {
              isChanged = true;
              Scaffold.of(context).showSnackBar(
                  SnackBar(content: Text("\"${item['title']}\" was removed from your plans")));
              setState(() {
                selectedChecklists['onList'].removeAt(index);
                DateTime date = DateTime.now();
                selectedChecklists['lastChange'] = date.toString();
              });
            },
            // Show a red background as the item is swiped away.
            child: Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              getCategory(item),
                              getTitle(item),
                              getDesc(item),
                            ],
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            showProgressBar(item),
                            Text(
                              "Completion",
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        )
                      ],
                    )),
              ),
            ),
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var selectedChecklists = DummyUser.loggedUser['checklist'];

    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    // TODO: implement build
    return WillPopScope(
      onWillPop: () {
        backToMyPlans();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.notWhite,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: AppTheme.blueTheme,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => backToMyPlans(),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  goToAddChecklist();
                }),
          ],
        ),
        body: ListView(
          shrinkWrap: true,
          controller: controller,
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Your Current Daily Checklist Information',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Last Changed',
                            style: TextStyle(fontSize: 14.0),
                          ),
                          Text(
                            format.format(DateTime.parse(DummyUser.loggedUser['checklist']['lastChange'])),
                            style: TextStyle(fontSize: 14.0),
                          )
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Average Checklist Items Passed',
                            style: TextStyle(fontSize: 14.0),
                          ),
                          Text(
                            '3 items',
                            style: TextStyle(fontSize: 14.0),
                          )
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              'Total completing all Daily Checklist',
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                          Text(
                            '0 times',
                            style: TextStyle(fontSize: 14.0),
                          )
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              'Easiest Checklist Item',
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                "Morning Dew",
                                style: TextStyle(fontSize: 14.0),
                              ),
                              Text(
                                "(11 times)",
                                style: TextStyle(fontSize: 14.0),
                              )
                            ],
                          )
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              'Hardest Checklist Item',
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                "Become a Book Worm",
                                style: TextStyle(fontSize: 14.0),
                              ),
                              Text(
                                "(1 time)",
                                style: TextStyle(fontSize: 14.0),
                              )
                            ],
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Divider(
                      color: AppTheme.grey,
                    ),
                  ),
                  Text(
                    'Current Checklist Target. Swipe card to delete from your plans',
                    style: TextStyle(color: AppTheme.grey, fontSize: 12),
                  ),
                  Expanded(
                    child: Divider(
                      color: AppTheme.grey,
                    ),
                  ),
                ],
              ),
            ),
            willWeShowTheList(selectedChecklists['onList'].length, selectedChecklists),
          ],
        ),
      ),
    );
  }
}
