import 'package:flutter/material.dart';
import 'package:myhealthdiary/appTheme.dart';

import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/register/RegisterPage.dart';
import 'package:myhealthdiary/main/HomePage.dart';

class ForgotPass extends StatefulWidget {
  static String tag = 'forgot-pass-page';
  @override
  _ForgotPassState createState() => new _ForgotPassState();
}

class _ForgotPassState extends State<ForgotPass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Center(
            child: Container(
              padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 20.0),
              child: Text(
                'Check your email to verificate new password. Thank you. ',
                style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
              ),
            ),
      ),
    );
  }
}


