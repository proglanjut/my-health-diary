import 'package:flutter/material.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/database/DummyUser.dart';

import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/register/RegisterPage.dart';
import 'package:myhealthdiary/main/HomePage.dart';

import 'ForgotPass.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _email;
  String _password;
  bool _isHidePassword = true;
  void _togglePasswordVisibility() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      bool foundUser = false;
      //Loop trough user dummy map database in ummyUser.dart

      DummyUser.userData.forEach((user) {
        String checkEmail = user['email'], checkPass = user['password'];

        //Debugging, print in Run Terminal: (Email and Password in iteration) vs (User input)
        print('$checkEmail vs $_email');
        print('$checkPass vs $_password');
        print('------');

        if (_email == checkEmail && _password == checkPass) {
          //This map (loggedUser) will be used in later pages
          // DummyUser.loggedUser.clear();

          DummyUser.loggedUser.addAll(user);
          //Tell the program not to show window
          foundUser = true;
          //Debugging: Username that match
          print(DummyUser.loggedUser['username']);
          //Go to HomePage
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomePage(2)),
            (Route<dynamic> route) => false,
          );
        }
      });

      //If user info not match with any data
      if (!foundUser) _userNotFound();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }
//  final emailController = TextEditingController();

  Future<void> _userNotFound() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Text('Error'),
          content: SingleChildScrollView(
            child: Text(
                'Sorry, we couldn\'t find a user related to your information'),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Okay, thank\'s',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    DummyUser.userData.forEach((user) {
      print(user.toString());
    });

    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 130.0,
        child: Image.asset('assets/images/logo-transparent.png'),
      ),
    );

    final notyetSignUp = FlatButton(
      child: Text(
        'Not a member? Register now',
        style: TextStyle(
          color: Colors.blue[400],
          fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return SignUpPage();
            },
          ),
        );
      },
    );

    final forgotPass = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(
          color: Colors.blue[400],
          fontWeight: FontWeight.bold,
          fontSize: 12.0,
        ),
      ),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return ForgotPass();
            },
          ),
        );
      },
    );

//    @override
//    void dispose() {
//      // Clean up the controller when the widget is disposed.
//      myController.dispose();
//      super.dispose();
//    }

    String validatePass(String value) {
      if (value.length < 6)
        return 'Invalid password';
      else
        return null;
    }

    String validateEmail(String value) {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Invalid email';
      else
        return null;
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 20.0),
          children: <Widget>[
            logo,
            SizedBox(height: 30.0),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    autofocus: false,
                    onChanged: (emailVal) {
                      setState(() => _email = emailVal);
                    },
                    decoration: InputDecoration(
                      hintText: 'Email',
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                    ),
                    validator: validateEmail,
                    onSaved: (String val) {
                      _email = val;
                    },
                  ),
                  SizedBox(height: 8.0),
                  TextFormField(
                    obscureText: _isHidePassword,
                    autofocus: false,
                    onChanged: (passwordVal) {
                      setState(() => _password = passwordVal);
                    },
                    initialValue: '',
                    keyboardType: TextInputType.text,
                    validator: validatePass,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      suffixIcon: GestureDetector(
                        onTap: () {
                          _togglePasswordVisibility();
                        },
                        child: Icon(
                          _isHidePassword
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: _isHidePassword ? Colors.grey : Colors.blue,
                        ),
                      ),
                      isDense: true,
                    ),
                  ),
                  SizedBox(height: 24.0),
                  Ink(
                    decoration: const BoxDecoration(
                      color: AppTheme.blue,
                      borderRadius: BorderRadius.all(
                        Radius.circular(5.0),
                      ),
                    ),
                    child: MaterialButton(
                      minWidth: double.infinity,
                      height: 48.0,
                      onPressed: _validateInputs,
                      //color: Colors.lightBlueAccent,
                      child: Text('Login',
                          style:
                              TextStyle(fontSize: 22.0, color: Colors.white)),
                    ),
                  ),
                ],
              ),
            ),
            notyetSignUp,
            forgotPass,
          ],
        ),
      ),
    );
  }
}
