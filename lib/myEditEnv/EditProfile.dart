import 'package:flutter/services.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myhealthdiary/main/MyProfile.dart';
import 'package:myhealthdiary/main/Settings.dart';
import 'package:myhealthdiary/register/RegisterActivityPage.dart';

class EditProfile extends StatefulWidget {
  String get title => "Edit Profile";

  @override
  State<StatefulWidget> createState() {
    return _EditProfileState();
  }

}
enum Activity { notact, lightly, active, veryact }
Activity _activity = Activity.notact;

class _EditProfileState extends State<EditProfile>{
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;
  TextEditingController dateController = TextEditingController();

  bool _status = true, isChanged = false;
  final FocusNode myFocusNode = FocusNode();
  DateTime selectedDate = DateTime.now();
  
  static Map<String, dynamic> userInfo = DummyUser.loggedUser;
  List<Map<String, dynamic>> userDatabase = DummyUser.userData;

  String username = userInfo['username'], 
    email = userInfo['email'],
    birthday = userInfo['birthday'],
    _valGender = userInfo['gender'],
    userActivity = userInfo['activity'];
  int weight = userInfo['weight'],
    height = userInfo['height'];

  List _listGender = ["Male","Female", "Other"];
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1970),
        lastDate: DateTime(2100));
    if (picked != null)
      setState(() {
        selectedDate = picked;
        birthday = picked.toString();
        dateController.text = selectedDate.year.toString() +
            '/ ' +
            selectedDate.month.toString() +
            '/ ' +
            selectedDate.day.toString();
      });
  }

  final _newUname = TextEditingController();
  final _newEmail = TextEditingController();
  final _newHeight = TextEditingController();
  final _newWeight = TextEditingController();
  final _newBodyActivity = TextEditingController();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    switch(userActivity){
      case 'Not Very Active':
        _activity = Activity.notact;
        break;
      case 'Lightly Active':
        _activity = Activity.lightly;
        break;
      case 'Active':
        _activity = Activity.active;
        break;
      case 'Very Active':
        _activity = Activity.veryact;
        break;
    }

    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    final newUnameField = TextFormField(
        obscureText: false,
        onChanged: (String value) {
          isChanged = true;
          setState(() => username = value);
        },
        style: new TextStyle(fontSize: 17.0),
        initialValue: username,
//        controller: _newUname,
        decoration: InputDecoration(
          hintText: "Enter your new username",
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ));
    final newEmailField = TextFormField(
        obscureText: false,
        onChanged: (String value) {
          isChanged = true;
          setState(() => email = value);
        },
        style: new TextStyle(fontSize: 17.0),
        initialValue: email,
//        controller: _newEmail,
        decoration: InputDecoration(
          hintText: "Enter your new email address",
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ));
    final newHeightField = TextFormField(
        obscureText: false,
        keyboardType: TextInputType.number,
        onChanged: (String value) {
          int numValue = int.parse(value);
          print('Height: $numValue');
          isChanged = true;
          setState(() => height = numValue);
        },
        style: new TextStyle(fontSize: 17.0),
        initialValue: height.toString(),
//        controller: _newHeight,
        decoration: InputDecoration(
          hintText: "Enter your new height",
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ));
    final newWeightField = TextFormField(
        obscureText: false,
        keyboardType: TextInputType.number,
        inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
        onChanged: (String value) {
          int numValue = int.parse(value);
          print('Weight: $numValue');
          isChanged = true;
          setState(() => weight = numValue);
        },
        style: new TextStyle(fontSize: 17.0),
        initialValue: weight.toString(),
//        controller: _newWeight,
        decoration: InputDecoration(
          hintText: "Enter your new weight",
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ));
//    final newBodyActivityField = TextFormField(
//        obscureText: false,
//        style: new TextStyle(fontSize: 17.0),
//        controller: _newBodyActivity,
//        decoration: InputDecoration(
//          hintText: "Enter your new body activity",
//          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
//        ));
    return WillPopScope(
      onWillPop: () {
        isChanged ? returnConfirmation() : moveToLastScreen();
      },
      child: Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: Colors.lightBlue[800],
        ),
        body: ListView(
          shrinkWrap: true,
          controller: controller,
          padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 20.0),
          children: <Widget>[
            Container(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: new Stack(fit: StackFit.loose, children: <Widget>[
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          width: 140.0,
                          height: 140.0,
                          decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              image: new ExactAssetImage('assets/images/logo-circle.png'),
                              fit: BoxFit.cover,
                            ),
                          )
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 90, right: 100.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new CircleAvatar(
                          backgroundColor: Colors.red,
                          radius: 25.0,
                          child: new Icon(
                            Icons.camera_alt,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],),
              ),
            ),
            Container(
              color: Color(0xffffffff),
              child: Padding(
                  padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 20.0),
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Username',
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              )
                            ],
                        ),
                        newUnameField,
                        SizedBox(height: 20.0,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Email Address',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        newEmailField,
                        SizedBox(height: 20,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Birthdate',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 45,
                          child: TextField(
                            onTap: () => _selectDate(context),
                            controller: dateController,
                            decoration: const InputDecoration(
                              suffix: Icon(
                                Icons.calendar_today,
                              ),
                              hintText: 'YYYY/ MM/ DD',
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Gender',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        DropdownButton(
                          hint: Text("Select your new gender"),
                          value: _valGender,
                          items: _listGender.map((value) {
                            return DropdownMenuItem(
                              child: Text(value),
                              value: value,
                            );
                          }).toList(),
                          onChanged: (value) {
                            isChanged = true;
                            setState(() {
                              _valGender = value;
                            });
                          },
                        ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Weight',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        newWeightField,
                        SizedBox(height: 20,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Height',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        newHeightField,
                        SizedBox(height: 20,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Body Activity',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 15,),
                              ],
                            )
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: Activity.notact,
                                  groupValue: _activity,
                                  onChanged: (Activity value) {
                                    isChanged = true;
                                    setState(() {
                                      _activity = value;
                                      userActivity = 'Not Very Active';
                                    });
                                  },
                                ),
                                Text(
                                  "Not Very Active",
                                  style: TextStyle(fontSize: 17.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: Activity.lightly,
                                  groupValue: _activity,
                                  onChanged: (Activity value) {
                                    isChanged = true;
                                    setState(() {
                                      _activity = value;
                                      userActivity = 'Lightly Active';
                                    });
                                  },
                                ),
                                Text(
                                  "Lightly Active",
                                  style: TextStyle(fontSize: 17.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: Activity.active,
                                  groupValue: _activity,
                                  onChanged: (Activity value) {
                                    isChanged = true;
                                    setState(() {
                                      _activity = value;
                                      userActivity = 'Active';
                                    });
                                  },
                                ),
                                Text(
                                  "Active",
                                  style: TextStyle(fontSize: 17.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: Activity.veryact,
                                  groupValue: _activity,
                                  onChanged: (Activity value) {
                                    isChanged = true;
                                    setState(() {
                                      _activity = value;
                                      userActivity = 'Very Active';
                                    });
                                  },
                                ),
                                Text(
                                  "Very Active",
                                  style: TextStyle(fontSize: 17.0),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 30.0,),
                        Center(
                          child: RaisedButton(
                            child: new Text("Save"),
                            textColor: Colors.white,
                            color: Colors.green,
                            onPressed: () {
                              _savedConfirmation();
                            },
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(20.0)
                            ),
                          ),
                        )
                      ]
                  )
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  Future<bool> returnConfirmation() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        contentPadding: EdgeInsets.all(30.0),
        title: Text('Confirmation'),
        content: Text('Are you sure want to exit? Your changes will not saved'),
        actions: <Widget>[
          MaterialButton(
            child: Text('Yes',
              style: TextStyle(
                color: Colors.red,
              ),
            ),
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomePage(4)),
                    (Route<dynamic> route) => false,
              );
            },
          ),
          MaterialButton(
            child: Text('No',
              style: TextStyle(
                color: AppTheme.blueTheme,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    ) ??
        false;
  }

  Future<bool> _savedConfirmation() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        contentPadding: EdgeInsets.all(30.0),
        title: Text('Confirmation'),
        content: Text('Are you sure you want to save your changes?'),
        actions: <Widget>[
          MaterialButton(
            child: Text('Yes',
              style: TextStyle(
                color: AppTheme.blueTheme,
              ),
            ),
            onPressed: () {userInfo['username'] = username;
            userInfo['email'] = email;
            userInfo['birthday'] = birthday;
            userInfo['gender'] = _valGender;
            userInfo['height'] = height;
            userInfo['weight'] = weight;
            userInfo['activity'] = userActivity;
            userDatabase.forEach((user){
              if(user['id'] == userInfo['id']){
                user['username'] = username;
                user['email'] = email;
                user['birthday'] = birthday;
                user['gender'] = _valGender;
                user['height'] = height;
                user['weight'] = weight;
                user['activity'] = userActivity;
                scaffoldKey.currentState.showSnackBar(
                    SnackBar(content: Text("Your profile successfully updated!")));
              }
            });
            isChanged = false;
            Navigator.of(context).pop(true);

            },
          ),
          MaterialButton(
            child: Text('No',
              style: TextStyle(
                color: Colors.red,
              ),
            ),
            onPressed: () => Navigator.of(context).pop(false),
          ),
        ],
      ),
    ) ??
        false;
  }
}
