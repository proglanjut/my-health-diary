import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/services.dart';

import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/main/MyProfile.dart';
import 'package:myhealthdiary/welcome/SplashScreen.dart';
import 'package:myhealthdiary/register/RegisterPlansPage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
      .then((_) => runApp(new MyApp()));
  // runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
      Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));
    return new MaterialApp(
      title: 'My Health Diary',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColorDark: Colors.lightBlue[800],
        textTheme: AppTheme.textTheme,
        platform: TargetPlatform.iOS,
      ),
     home: SplashScreen(),
//    home: MyProfile(),
    // home: RegisterPlans()
    );
  }
}


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
