import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:sticky_headers/sticky_headers.dart';

import 'DataForAll.dart';

class AddWorkout extends StatefulWidget {
  String get title => "Add New Workout";

  @override
  _AddWorkoutState createState() => _AddWorkoutState();
}

class _AddWorkoutState extends State<AddWorkout> {
  TextEditingController editingController;
  ScrollController controller;
  final workout = DataForAll.workoutData;
  List<Map<String, Object>> items = List<Map<String, Object>>();

  @override
  void initState() {
    items.addAll(workout);
    super.initState();
  }

  //Method untuk mencari nama olahraga yang menganung string yang diinput user di article search bar
  void filterSearchResults(String query) {
    List<Map<String, Object>> dummySearchList = List<Map<String, Object>>();
    dummySearchList.addAll(workout);
    if (query.isNotEmpty) {
      List<Map<String, Object>> dummyListData = List<Map<String, Object>>();
      dummySearchList.forEach((item) {
        if (item.containsKey('name')) {
          if (item['name'].toString().toLowerCase().contains(query.toLowerCase())) {
            dummyListData.add(item);
          }
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(workout);
      });
    }
  }

  //Kembali ke my diary lewat HomePage
  void backToMyDiary() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => HomePage(1)),
      (Route<dynamic> route) => false,
    );
  }

  //Kembali ke layar sebelumnya
  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  //Method untuk memilih antara menampilkan list atau menampilkan pesan error
  //jika tidak ada nama olahraga yang sesuai user input
  Widget willWeShowTheList(int count) {
    if (count == 0) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(":'(",
                style: TextStyle(
                    color: AppTheme.blueTheme,
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold
                )
            ),
            SizedBox(height: 20,),
            Text("Sorry we can't find the workout that you are looking for",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
        shrinkWrap: true,
        itemCount: items.length,
        scrollDirection: Axis.vertical,
        controller: controller,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return createWorkoutCard(items[index]);
        },
      );
    }
  }

  //Membuat kartu untuk setiap olahraga
  Widget createWorkoutCard(data) {
    String name = data['name'];
    bool length = data['length'], 
        duration = data['duration'], 
        weight = data['weight'], 
        count = data['count'], 
        speed = data['speed'];

    List<Widget> additional = [];
    if(length) additional.add(Icon(MaterialCommunityIcons.map_marker_distance, color: AppTheme.blueTheme,));
    if(duration) additional.add(Icon(Entypo.stopwatch, color: AppTheme.blueTheme));
    if(weight) additional.add(Icon(MaterialCommunityIcons.weight, color: AppTheme.blueTheme));
    if(count) additional.add(Icon(MaterialCommunityIcons.counter, color: AppTheme.blueTheme));
    if(speed) additional.add(Icon(MaterialCommunityIcons.speedometer, color: AppTheme.blueTheme));

    return Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: GestureDetector(
          onTap: () {
            _confirmationDialog(length, duration, weight, count, speed);
          },
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 5.0),
                    child: Text(
                      name,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Additional Information"),
                      Row(
                        children: additional,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        )
    );
  }

  //Memunculkan jendela konfirmasi ketika user tap salah satu card dan tanyakan
  //informasi tambahan sesuai yang ada di "Additional Information"
  Future<void> _confirmationDialog(length, duration, weight, count, speed) async {
    List<Widget> whatInfo = [
      Text('Tell us some additional information!')
    ];
    if(length) whatInfo.add(
      TextFormField(
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Length (in kilometers)',
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ),
      ),
    );
    if(duration) whatInfo.add(
      TextFormField(
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Duration (in minutes)',
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ),
      ),
    );
    if(weight) whatInfo.add(
      TextFormField(
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Weight (in kilograms)',
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ),
      ),
    );
    if(count) whatInfo.add(
      TextFormField(
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Count',
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ),
      ),
    );
    if(speed) whatInfo.add(
      TextFormField(
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Speed (in kph)',
          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
        ),
      ),
    );

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: whatInfo
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Done',
                style: TextStyle(
                  color: AppTheme.blueTheme,
                ),
              ),
              onPressed: () {
                backToMyDiary();
              },
            ),
            FlatButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: toolbar,
        backgroundColor: AppTheme.blueTheme,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              moveToLastScreen();
            }),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: controller,
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          StickyHeader(
            header: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: TextFormField(
                autofocus: false,
                onChanged: (value) {
                  filterSearchResults(value);
                },
                controller: editingController,
                decoration: InputDecoration(
                    hintText: 'Search workout...',
                    contentPadding: EdgeInsets.all(5.0),
                    prefixIcon: Icon(
                      Icons.search,
                      size: 14,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)))),
              ),
            ),
            content: willWeShowTheList(items.length)
          )
        ],
      ),
    );
  }
}
