import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:sticky_headers/sticky_headers.dart';

import 'DataForAll.dart';

class AddWater extends StatefulWidget{
  String get title => "Add New Drink";

  @override
  _AddWaterState createState() => _AddWaterState();
}

class _AddWaterState extends State<AddWater>{
  ScrollController controller;
  TextEditingController editingController;
  final water = DataForAll.waterData;
  List<String> items = List<String>();

  @override
  void initState() {
    items.addAll(water);
    super.initState();
  }

  //Method untuk mencari nama minuman yang menganung string yang diinput user di article search bar
  void filterSearchResults(String query) {
    List<String> dummySearchList = List<String>();
    dummySearchList.addAll(water);
    if (query.isNotEmpty) {
      List<String> dummyListData = List<String>();
      dummySearchList.forEach((item) {
        print(query +" " +item);
        if (item.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(water);
      });
    }
  }

  //Kembali ke my diary lewat homepage
  void backToMyDiary (){
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => HomePage(1)),
          (Route<dynamic> route) => false,
    );
  }

  //Kembali ke layar sebelumnya
  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  //Munculkan jendela konfirmasi dan tanyakan berapa banyak dia minum ketika
  //user tap cardnya
  Future<void> _confirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Tell us how much you drink'),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                    hintText: 'Volume (in mililiters)',
                    contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Done',
                style: TextStyle(
                  color: AppTheme.blueTheme,
                ),
              ),
              onPressed: () {
                backToMyDiary();
              },
            ),
            FlatButton(
              child: Text('Cancel',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  //Method untuk memilih antara menampilkan list atau menampilkan pesan error
  //jika tidak ada nama minuman yang sesuai user input
  Widget willWeShowTheList(int count) {
    if (count == 0) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(":'(",
                style: TextStyle(
                    color: AppTheme.blueTheme,
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold
                )
            ),
            SizedBox(height: 20,),
            Text("Sorry we can't find the drink that you are looking for",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
        shrinkWrap: true,
        itemCount: items.length,
        scrollDirection: Axis.vertical,
        controller: controller,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return createWaterCard(items[index]);
        },
      );
    }
  }

  //Method untuk membuat kartu untuk setiap minuman
  Widget createWaterCard(data){
    String name = data;
    return Padding(
        padding: EdgeInsets.only(
            bottom: 10
        ),
        child: GestureDetector(
          onTap: () {
            _confirmation();
          },
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    name,
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.local_cafe, color: AppTheme.blueTheme),
                      Icon(Icons.local_bar, color: AppTheme.blueTheme)
                    ],
                  )
                ],
              ),
            ),
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: toolbar,
        backgroundColor: AppTheme.blueTheme,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              moveToLastScreen();
            }),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: controller,
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
            StickyHeader(
              header: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  autofocus: false,
                  onChanged: (value) {
                    filterSearchResults(value);
                  },
                  controller: editingController,
                  decoration: InputDecoration(
                    hintText: 'Search drink...',
                    contentPadding: EdgeInsets.all(5.0),
                    prefixIcon: Icon(Icons.search,
                      size: 14,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))
                    )
                  ),
                ),
              ),
              content: willWeShowTheList(items.length)
            )
        ],
      ),
    );
  }
}