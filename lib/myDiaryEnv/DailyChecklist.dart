import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
import 'package:flutter/widgets.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/myPlansEnv/ChecklistItems.dart';
import 'package:myhealthdiary/myPlansEnv/DailyChecklistSettings.dart';
import 'package:percent_indicator/percent_indicator.dart';

class TodayChecklist extends StatefulWidget {
  String get title => "Daily Checklist";

  @override
  _DailyChecklistState createState() => _DailyChecklistState();
}

class _DailyChecklistState extends State<TodayChecklist> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;
  int finishedItem = 0, totalItem = 0;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<Map<String, dynamic>> userData = DummyUser.userData;

  Widget createCard(data) {
    String title = data['title'], desc = data['description'];
    bool isFinished = data['finished'];

    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        margin: EdgeInsets.all(2),
        child: CheckboxListTile(
          title: Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
          ),
          subtitle: Text(
            desc,
            style: TextStyle(fontSize: 15.0),
          ),
          value: isFinished,
          onChanged: (bool value) {
            setState(() {
              data['finished'] = value;
              userData.forEach((item) {
                if(DummyUser.loggedUser['id'] == item['id']){
                  item['checklist']['onList'].forEach((checklist){
                    if(checklist['id'] == data['id']){
                      if(value){
                        checklist['completion']++;
                      }else{
                        checklist['completion']--;
                      }
                    }
                  });
                }
              });
            });
          },
        ),
      ),
    );
  }

  Widget willWeShowTheList(int count, List<Map<String, dynamic>> selectedChecklists){
    finishedItem = 0;
    selectedChecklists.forEach((item){
      bool isFinished = item['finished'];
      if(isFinished) finishedItem++;
    });

    double percent = finishedItem/totalItem;
    if(count == 0){
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(":'(",
                style: TextStyle(
                    color: AppTheme.blueTheme,
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold
                )
            ),
            SizedBox(height: 20,),
            Text("You didn't have any checklist items",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            ),
            SizedBox(height: 20),
            FlatButton(
              color: AppTheme.blueTheme,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return DailyChecklist(); //UNCOMMENT WHEN PAGE IS DONE
                    },
                  ),
                );
              },
              child: Text('Set Them Now!'),
              textColor: Colors.white,
            ),
          ],
        ),
      );
    }else{
      return Container(
          margin: EdgeInsets.all(10.0),
          child: ListView(
            shrinkWrap: true,
            controller: controller,
            padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 20.0),
            children: <Widget>[
              Container(width: 70.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularPercentIndicator(
                      radius: 120.0,
                      lineWidth: 10.0,
                      animation: true,
                      percent: percent,
                      center: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('$finishedItem',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 37.0)),
                          Text('Completed',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15.0))
                        ],
                      ),
                      backgroundColor: AppTheme.progress,
                      progressColor: (percent == 1.0) ? Colors.green : AppTheme.blueTheme),
                ],
              ),
              ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: selectedChecklists.length,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, index) {
                  return createCard(selectedChecklists[index]);
                },
              )
            ],
          ),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    var selectedChecklists = DummyUser.loggedUser['checklist']['onList'];
    totalItem = selectedChecklists.length;

    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.notWhite,
        appBar: AppBar(
            title: toolbar,
            backgroundColor: Colors.lightBlue[800],
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  moveToLastScreen();
                }),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.create, color: Colors.white),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return DailyChecklist(); //UNCOMMENT WHEN PAGE IS DONE
                      },
                    ),
                  );
                },
              ),
            ]),
        body: willWeShowTheList(totalItem, selectedChecklists)
      ),
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
