import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:myhealthdiary/main/MyDiary.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class AddSleep extends StatefulWidget{
  String get title => "Add New Sleep";

  @override
  _AddSleepState createState() => _AddSleepState();
}

class _AddSleepState extends State<AddSleep>{
  ScrollController controller;

  void backToMyDiary (){
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => HomePage(1)),
          (Route<dynamic> route) => false,
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  Future<void> _confirmationDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Add this sleep duration to your diary?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                backToMyDiary();
              },
            ),
            FlatButton(
              child: Text('No',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  final TimeOfDay selectedTime = TimeOfDay.now();
  final ValueChanged<TimeOfDay> selectTime = null;
  Future<void> _TimeInput(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: selectedTime,
    );
    if(picked != null && picked != selectedTime) selectTime(picked);
  }

  @override
  Widget build(BuildContext context) {
    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: toolbar,
        backgroundColor: AppTheme.blueTheme,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            moveToLastScreen();
          },
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                _confirmationDialog();
              }
          )
        ],
      ),
      body: ListView(
        shrinkWrap: true,
        controller: controller,
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              decoration: InputDecoration(
                  hintText: 'From',
                  contentPadding: EdgeInsets.all(5.0),
                  prefixIcon: Icon(Icons.watch_later,
                    size: 14,
                  ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))
                  )
              ),
            ),
          ),
          Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              decoration: InputDecoration(
                  hintText: 'From',
                  contentPadding: EdgeInsets.all(5.0),
                  prefixIcon: Icon(Icons.watch_later,
                    size: 14,
                  ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))
                  )
              ),
            ),
          ),
        ],
      ),
    );
  }
}