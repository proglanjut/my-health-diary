class DataForAll{
  static final foodsData = [
    {
      'name': 'Rice',
      'qty': '1 Plate (70 grams)',
      'cal': 204.0,
      'carbo': 44.08,
      'fat': 1.3,
      'protein': 4.2
    },
    {
      'name': 'Rice',
      'qty': '1/2 Plate (35 grams)',
      'cal': 126.0,
      'carbo': 27.8,
      'fat': 0.2,
      'protein': 2.3
    },
    {
      'name': 'Rice',
      'qty': '1/4 Plate (17 grams)',
      'cal': 61.2,
      'carbo': 13.5,
      'fat': 0.1,
      'protein': 1.1
    },
    {
      'name': 'Bread',
      'qty': '1 Piece',
      'cal': 77.0,
      'carbo': 14.1,
      'fat': 0.9,
      'protein': 3.1
    },
    {
      'name': 'Potato',
      'qty': '1 Piece (213 grams)',
      'cal': 204.0,
      'carbo': 44.08,
      'fat': 1.3,
      'protein': 4.2
    },
    {
      'name': 'French Fries',
      'qty': '1 Pack (100 grams)',
      'cal': 311.9,
      'carbo': 48.7,
      'fat': 17.6,
      'protein': 4.1
    },
    {
      'name': 'Tempe',
      'qty': '1 Small Piece',
      'cal': 34.0,
      'carbo': 1.79,
      'fat': 2.28,
      'protein': 2.0
    },
    {
      'name': 'Tahu',
      'qty': '1 Small Piece',
      'cal': 147.0,
      'carbo': 1.36,
      'fat': 2.62,
      'protein': 2.23
    },
    {
      'name': 'Cereal',
      'qty': '1 Serving (33 grams)',
      'cal': 124.0,
      'carbo': 27.4,
      'fat': 1.12,
      'protein': 2.39
    },
    {
      'name': 'Noodle',
      'qty': '1 Serving (160 grams)',
      'cal': 219.0,
      'carbo': 40.02,
      'fat': 3.3,
      'protein': 7.2
    },
    {
      'name': 'Chicken Satay',
      'qty': '1 Piece',
      'cal': 34.0,
      'carbo': 0.73,
      'fat': 2.22,
      'protein': 2.93
    },
    {
      'name': 'Goat Satay',
      'qty': '1 Piece',
      'cal': 32.0,
      'carbo': 0.72,
      'fat': 2.11,
      'protein': 2.84
    },
  ];

  static final waterData = [
    'Plain Water',
    'Coffee',
    'Fruit Juice',
    'Syntetic Syrup'
    'Natural Syrup',
    'Tea',
    'Soda/Carbonated Drinks',
    'Cow Milk',
    'Goat Milk',
    'Soya Milk',
    'Ionized Water',
    'Honey Water',
    'Powder Drink',
    'Yoghurt',
    'Wine',
    'Alcohol Drinks',
    'Coconut Water',
    'Other Water'
  ];

  static final workoutData = [
    {
      'name': 'Walking',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Running',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Cycling',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Swimming in Pool',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Swimming in Current Waters',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Mountain Cycling',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Mountaineering',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Hand Cycling',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Weightlifting',
      'length': false,
      'duration': false,
      'weight': true,
      'count': true,
      'speed': false
    },
    {
      'name': 'Weight Holding',
      'length': false,
      'duration': true,
      'weight': true,
      'count': false,
      'speed': false
    },
    {
      'name': 'Static Cycling',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Treadmill Workout',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': true
    },
    {
      'name': 'Wheel Chair Movement',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Jogging',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Skipping',
      'length': false,
      'duration': true,
      'weight': false,
      'count': true,
      'speed': false
    },
    {
      'name': 'Running over Stairs',
      'length': true,
      'duration': true,
      'weight': false,
      'count': true,
      'speed': false
    },
    {
      'name': 'Otopet Riding',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Rolled Shoes Movement',
      'length': true,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Ice Skating',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Snow Ski',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Skateboarding',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Aerobic Movement',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Fencing',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Karate/Silat/Other Self-defencing',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Sailing',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Horse riding',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Softball',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Mediating',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Baseball',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Handball',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Volleyball',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Beach Volleyball',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Rafting or Paddling',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'American Football',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Frisbee',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Golf',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Classic Hockey',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Ice Hockey',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Cricket',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Kickboxing',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Dancing',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Diving',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Climbing',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Polo',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Water Polo',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Horse Polo',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Racquetball',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Rugby',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Surfing',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Gymnastics',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Football',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Tennis',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Squash',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Boxing',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Yoga',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
    {
      'name': 'Zumba',
      'length': false,
      'duration': true,
      'weight': false,
      'count': false,
      'speed': false
    },
  ];
}