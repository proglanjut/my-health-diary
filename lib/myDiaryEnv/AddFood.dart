import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:myhealthdiary/myDiaryEnv/DataForAll.dart';
import 'package:sticky_headers/sticky_headers.dart';

class AddFood extends StatefulWidget {
  String get title => "Add New Food";

  @override
  _AddFoodState createState() => _AddFoodState();
}

class _AddFoodState extends State<AddFood> {
  ScrollController controller;
  TextEditingController editingController;
  final foods = DataForAll.foodsData;
  List<Map<String, Object>> items = List<Map<String, Object>>();

  @override
  void initState() {
    items.addAll(foods);
    super.initState();
  }

  //Method untuk mencari nama makanan yang menganung string yang diinput user di article search bar
  void filterSearchResults(String query) {
    List<Map<String, Object>> dummySearchList = List<Map<String, Object>>();
    dummySearchList.addAll(foods);
    if (query.isNotEmpty) {
      List<Map<String, Object>> dummyListData = List<Map<String, Object>>();
      dummySearchList.forEach((item) {
        if (item.containsKey('name')) {
          if (item['name'].toString().toLowerCase().contains(query.toLowerCase())) {
            dummyListData.add(item);
          }
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(foods);
      });
    }
  }

  void backToMyDiary() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => HomePage(1)),
      (Route<dynamic> route) => false,
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  Future<void> _confirmationDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Text('Confirmation'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Add this food to your diary?.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                backToMyDiary();
              },
            ),
            FlatButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: AppTheme.blue,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  //Membuat kartu untuk setiap makanan
  Widget createFoodCard(data) {
    String name = data['name'], amount = data['qty'];
    double cal = data['cal'].toDouble(),
        carbo = data['carbo'].toDouble(),
        fat = data['fat'].toDouble(),
        protein = data['protein'].toDouble();

    return Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: GestureDetector(
          onTap: () {
            _confirmationDialog();
          },
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 5.0),
                    child: Text(
                      name,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[Text("Amount of Serving"), Text(amount)],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Calories"),
                      Text("${cal.toString()} gram")
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Carbohydrate"),
                      Text("${carbo.toString()} gram")
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Fat"),
                      Text("${fat.toString()} gram")
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Protein"),
                      Text("${protein.toString()} gram")
                    ],
                  )
                ],
              ),
            ),
          ),
        ));
  }

  //Method untuk memilih antara menampilkan list atau menampilkan pesan error
  //jika tidak ada nama makanan yang sesuai user input
  Widget willWeShowTheList(int count){
    if(count == 0){
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(":'(",
                style: TextStyle(
                    color: AppTheme.blueTheme,
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold
                )
            ),
            SizedBox(height: 20,),
            Text("Sorry we can't find the food that you are looking for",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
        shrinkWrap: true,
        itemCount: items.length,
        scrollDirection: Axis.vertical,
        controller: controller,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return createFoodCard(items[index]);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: toolbar,
        backgroundColor: AppTheme.blueTheme,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              moveToLastScreen();
            }),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: controller,
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          StickyHeader(
            header: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: TextFormField(
                autofocus: false,
                onChanged: (value) {
                  filterSearchResults(value);
                },
                controller: editingController,
                decoration: InputDecoration(
                    hintText: 'Search food...',
                    contentPadding: EdgeInsets.all(5.0),
                    prefixIcon: Icon(
                      Icons.search,
                      size: 14,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)))),
              ),
            ),
            content: willWeShowTheList(items.length),
          )
        ],
      ),
    );
  }
}
