class DummyUser {
  //If user login data match with some data in userData, put in loggedUser
  static Map<String, dynamic> loggedUser = {};

  //User static map database
  static List<Map<String, dynamic>> userData = [
    {
      'id': 1,
      'username': 'haqiunyu',
      'email': 'haqicantik@email.com',
      'password': '12345678',
      'birthday': '1960-02-29T00:00:00Z',
      'gender': 'Female',
      'height': 178,
      'weight': 70,
      'activity': 'low',
      'plans': {
        'bodyWeight': {
          'lastChange': '2020-05-01T00:00:00Z',
          'target': 100,
          'average': 0.4
        },
        'nutrition': {
          'lastChange': '2020-05-01T00:00:00Z',
          'calories': 2000,
          'carbo': 30,
          'fat': 50,
          'protein': 20,
        },
        'water': {
          'lastChange': '2020-05-01T00:00:00Z',
          'volume': 800
        },
        'sleep': {
          'lastChange': '2020-05-01T00:00:00Z',
          'day': 240,
          'night': 10
        },
        'workout': {
          'lastChange': '2020-05-01T00:00:00Z',
          'distance': 100.0,
          'duration': 60
        }
      },
      'checklist': {
        'lastChange': '2020-05-01T00:00:00Z',
        'onList': [
          {
            'id': 18,
            'title': 'Morning Dew',
            'description': 'Wake up before 6 am',
            'category': 'sleep',
            'completion': 11,
            'used': 11,
            'finished': true,
          },
          {
            'id': 23,
            'title': 'Become a Book Worm',
            'description': 'Read a book at least 10 pages',
            'category': 'habits',
            'completion': 1,
            'used': 11,
            'finished': false,
          },
          {
            'id': 19,
            'title': 'Sweet Dreams',
            'description': 'Go to bed before 10 pm',
            'category': 'sleep',
            'completion': 9,
            'used': 11,
            'finished': false,
          },
        ]
      }
    },
    {
      'id': 2,
      'username': 'bukanbakulcireng',
      'email': 'haqiganteng@email.com',
      'password': '12345678',
      'birthday': '2016-02-29T00:00:00Z',
      'gender': 'Female',
      'height': 178,
      'weight': 70,
      'activity': 'Active',
      'plans': {
        'bodyWeight': {
          'lastChange': '2020-05-01T00:00:00Z',
          'target': 100,
          'average': 0.4
        },
        'nutrition': {
          'lastChange': '2020-05-01T00:00:00Z',
          'calories': 2000,
          'carbo': 30,
          'fat': 50,
          'protein': 20,
        },
        'water': {
          'lastChange': '2020-05-01T00:00:00Z',
          'volume': 800
        },
        'sleep': {
          'lastChange': '2020-05-01T00:00:00Z',
          'day': 240,
          'night': 10
        },
        'workout': {
          'lastChange': '2020-05-01T00:00:00Z',
          'distance': 110.0,
          'duration': 60
        }
      },
      'checklist': {
        'lastChange': '2020-05-01T00:00:00Z',
        'onList': [
          {
            'id': 18,
            'title': 'Morning Dew',
            'description': 'Wake up before 6 am',
            'category': 'sleep',
            'completion': 11,
            'used': 11,
            'finished': true,
          },
          {
            'id': 23,
            'title': 'Become a Book Worm',
            'description': 'Read a book at least 10 pages',
            'category': 'habits',
            'completion': 1,
            'used': 11,
            'finished': false,
          },
          {
            'id': 30,
            'title': 'Morning Sunshine',
            'description': 'Sunbath at least 15 minutes before 9 am',
            'category': 'habits',
            'completion': 6,
            'used': 11,
            'finished': true,
          },
          {
            'id': 3,
            'title': 'Eat Fruit',
            'description': 'Eat any kind of fruit at least once',
            'category': 'food',
            'completion': 4,
            'used': 11,
            'finished': true,
          },
          {
            'id': 19,
            'title': 'Sweet Dreams',
            'description': 'Go to bed before 10 pm',
            'category': 'sleep',
            'active': true,
            'completion': 9,
            'used': 11,
            'finished': false,
          },
        ]
      }
    },
  ];

  //For creating new user during register
  static Map<String, dynamic> newUser = {
    'id': 0,
    'username': null,
    'email': null,
    'password': null,
    'birthday': null,
    'gender': null,
    'height': 0,
    'weight': 0,
    'activity': null,
    'plans': {
      'bodyWeight': {
        'lastChange': '',
        'target': 0,
        'average': 0.0
      },
      'nutrition': {
        'lastChange': '',
        'calories': 0,
        'carbo': 0,
        'fat': 0,
        'protein': 0,
      },
      'water': {
        'lastChange': '',
        'volume': 0
      },
      'sleep': {
        'lastChange': '',
        'day': 0,
        'night': 0
      },
      'workout': {
        'lastChange': '',
        'distance': 0.0,
        'duration': 0
      }
    },
    'checklist': {
      'lastChange': '',
      'onList': [
        {
          'id': 0,
          'title': 'Template'
        }
      ],
    }
  };
}