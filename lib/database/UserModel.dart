class User{
  String name, username, email, password, gender, activity, birthday;
  double height, weight;
  int id;
  Plans plans;

  User(this.name, this.username, this.email, this.password, this.gender,
      this.activity, this.birthday, this.height, this.weight, this.id,
      this.plans);

  Map<String, Object> toMap(){
    return{
      'id': id,
      'name': name,
      'username': username,
      'email': email,
      'password': password,
      'birthday': birthday,
      'gender': gender,
      'height': height,
      'weight': weight,
      'activity': activity,
      'plans': plans
    };
  }
}

class Plans{
  BodyWeightPlans bodyWeightPlans;
  NutritionPlans nutritionPlans;
  WorkoutPlans workoutPlans;
  WaterPlans waterPlans;
  SleepPlans sleepPlans;

  Plans(this.bodyWeightPlans, this.nutritionPlans, this.workoutPlans,
      this.waterPlans, this.sleepPlans);

  Map<String, dynamic> toMap(){
    return{
      'bodyWeight': bodyWeightPlans,
      'nutrition': nutritionPlans,
      'water': waterPlans,
      'sleep': sleepPlans,
      'workout': workoutPlans
    };
  }
}

class BodyWeightPlans{
  int target;
  double average;

  BodyWeightPlans({this.target, this.average});

  Map<String, dynamic> toMap(){
    return{
      'target': target,
      'average': average
    };
  }
}

class NutritionPlans{
  int calories, carbo, fat, protein;

  NutritionPlans({this.calories, this.carbo, this.fat, this.protein});

  Map<String, dynamic> toMap(){
    return{
      'calories': calories,
      'carbo': carbo,
      'fat': fat,
      'protein': protein,
    };
  }
}

class SleepPlans{
  int day, night;

  SleepPlans({this.day, this.night});

  Map<String, dynamic> toMap(){
    return{
      'day': day,
      'night': night
    };
  }
}

class WaterPlans{
  int volume;

  WaterPlans({this.volume});

  Map<String, dynamic> toMap(){
    return{
      'volume': volume
    };
  }
}

class WorkoutPlans{
  double distance;
  int duration;

  WorkoutPlans(this.distance, this.duration);

  Map<String, dynamic> toMap(){
    return{
      'distance': distance,
      'duration': duration
    };
  }
}

