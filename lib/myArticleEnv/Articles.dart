class Articles {
  Source source;
  String title;
  String description;
  String url;
  String urlToImage;
  String publishedAt;

  Articles(
      {this.source,
        this.title,
        this.description,
        this.url,
        this.urlToImage,
        this.publishedAt
      });

  factory Articles.fromJson(Map<String, dynamic> json) {
    return Articles(
        source: Source.fromJson(json["source"]),
        title: json["title"],
        description: json["description"],
        url: json["url"],
        urlToImage: json["urlToImage"],
        publishedAt: json["publishedAt"]
    );
  }
}

class Source {
  String id;
  String name;

  Source({this.id, this.name});

  factory Source.fromJson(Map<String, dynamic> json) {
    return Source(
      id: json["id"] as String,
      name: json["name"] as String,
    );
  }
}