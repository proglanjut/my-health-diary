import 'package:flutter/material.dart';
import 'package:myhealthdiary/appTheme.dart';

import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:myhealthdiary/register/RegisterYouPage.dart';
import 'package:myhealthdiary/login/LoginPage.dart';
import 'package:myhealthdiary/database/DummyUser.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _uname;
  String _email;
  String _password;
  bool _isHidePassword = true;
  void _togglePasswordVisibility() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }

  //    @override
//    void dispose() {
//      // Clean up the controller when the widget is disposed.
//      myController.dispose();
//      super.dispose();
//    }
  void _validateInputs() {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();

      DummyUser.loggedUser.addAll(DummyUser.newUser);
      int id = DummyUser.userData.last['id'];

      DummyUser.loggedUser['id'] = (id+1);
      DummyUser.loggedUser['username'] = _uname;
      DummyUser.loggedUser['email'] = _email;
      DummyUser.loggedUser['password'] = _password;
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return SignUpYou();
          },
        ),
      );
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 130.0,
        child: Image.asset('assets/images/logo-transparent.png'),
      ),
    );

    final alreadyLabel = FlatButton(
      child: Text(
        'Already have an account?',
        style: TextStyle(
          color: Colors.blue[400],
          fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return LoginPage();
            },
          ),
        );
      },
    );

    String validateUname(String value) {
      if (value.length < 5)
        return 'Invalid username, minimal 5 characters';
      else
        return null;
    }

    String validatePass(String value) {
      if (value.length < 6)
        return 'Invalid password';
      else
        return null;
    }

    String validateEmail(String value) {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Invalid email';
      else
        return null;
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 20.0),
          children: <Widget>[
            logo,
            SizedBox(height: 30.0),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    autofocus: false,
                    decoration: InputDecoration(
                      hintText: 'Username',
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                    ),
                    validator: validateUname,
                    onSaved: (String val) {
                      _uname = val;
                    },
                  ),
                  SizedBox(height: 8.0),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    autofocus: false,
                    decoration: InputDecoration(
                      hintText: 'Email',
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                    ),
                    validator: validateEmail,
                    onSaved: (String val) {
                      _email = val;
                    },
                  ),
                  SizedBox(height: 8.0),
                  TextFormField(
                    obscureText: _isHidePassword,
                    autofocus: false,
                    initialValue: '',
                    keyboardType: TextInputType.text,
                    onSaved: (value) {
                      setState(() {
                        _password = value;
                      });
                    },
                    validator: validatePass,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      suffixIcon: GestureDetector(
                        onTap: () {
                          _togglePasswordVisibility();
                        },
                        child: Icon(
                          _isHidePassword
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: _isHidePassword ? Colors.grey : Colors.blue,
                        ),
                      ),
                      isDense: true,
                    ),
                  ),
                  SizedBox(height: 24.0),
                  Ink(
                    decoration: const BoxDecoration(
                      color: AppTheme.blue,
                      borderRadius: BorderRadius.all(
                        Radius.circular(5.0),
                      ),
                    ),
                    child: MaterialButton(
                      minWidth: double.infinity,
                      height: 48.0,
                      onPressed: _validateInputs,
                      //color: Colors.lightBlueAccent,
                      child: Text('Register Now',
                          style:
                              TextStyle(fontSize: 22.0, color: Colors.white)),
                    ),
                  ),
                ],
              ),
            ),
            alreadyLabel,
          ],
        ),
      ),
    );
  }
}
