import 'dart:ffi';

import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myhealthdiary/register/AccountCreated.dart';
import 'package:myhealthdiary/register/RegisterChecklist.dart';
import 'package:myhealthdiary/database/DummyUser.dart';

class RegisterPlans extends StatefulWidget {
  String get title => "Plans";

  @override
  State<StatefulWidget> createState() {
    return _RegisterPlansState();
  }
}

class _RegisterPlansState extends State<RegisterPlans> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;

  bool _autoValidate = false;
  String _target;
  String _gl;
  String _cal;
  String _carbo;
  String _fat;
  String _protein;
  String _drink;
  String _days;
  String _nights;
  String _distance;
  String _duration;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      String todays = DateTime.now().toString();
      DummyUser.loggedUser['plans']['bodyWeight']['lastChanged'] = todays;
      DummyUser.loggedUser['plans']['nutrition']['lastChanged'] = todays;
      DummyUser.loggedUser['plans']['water']['lastChanged'] = todays;
      DummyUser.loggedUser['plans']['sleep']['lastChanged'] = todays;
      DummyUser.loggedUser['plans']['workout']['lastChanged'] = todays;
      //Body Weight
      DummyUser.loggedUser['plans']['bodyWeight']['target'] = int.parse(_target);
      DummyUser.loggedUser['plans']['bodyWeight']['average'] = double.parse(_gl);
      //Nutrition
      DummyUser.loggedUser['plans']['nutrition']['calories'] = int.parse(_cal);
      DummyUser.loggedUser['plans']['nutrition']['carbo'] = int.parse(_carbo);
      DummyUser.loggedUser['plans']['nutrition']['fat'] = int.parse(_fat);
      DummyUser.loggedUser['plans']['nutrition']['protein'] = int.parse(_protein);
      //Water
      DummyUser.loggedUser['plans']['water']['volume'] = int.parse(_drink);
      //Sleep
      DummyUser.loggedUser['plans']['sleep']['day'] = int.parse(_days);
      DummyUser.loggedUser['plans']['sleep']['night'] = int.parse(_nights);
      //Workout
      DummyUser.loggedUser['plans']['workout']['distance'] = double.parse(_distance);
      DummyUser.loggedUser['plans']['workout']['duration'] = int.parse(_duration);

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return RegisterChecklist();
          },
        ),
      );
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String validateTarget(String value) {
      if (value.length == 0)
        return 'Fill in your target';
      else
        return null;
    }

    String validateGl(String value) {
      if (value.length == 0)
        return 'Fill in your daily gain/lost average';
      else
        return null;
    }

    String validateCal(String value) {
      if (value.length == 0)
        return 'Fill in your calories';
      else
        return null;
    }

    String validateCarbo(String value) {
      if (value.length == 0)
        return 'Fill in your percentage of carbohydrate';
      else
        return null;
    }

    String validateFat(String value) {
      if (value.length == 0)
        return 'Fill in your percentage of fat';
      else
        return null;
    }

    String validateProtein(String value) {
      if (value.length == 0)
        return 'Fill in your percentage of protein';
      else
        return null;
    }

    String validateDrink(String value) {
      if (value.length == 0)
        return 'Fill in your water consumed volume';
      else
        return null;
    }

    String validateDays(String value) {
      if (value.length == 0)
        return 'Fill in your day sleep duration';
      else
        return null;
    }

    String validateNights(String value) {
      if (value.length == 0)
        return 'Fill in your night sleep duration';
      else
        return null;
    }

    String validateDistance(String value) {
      if (value.length == 0)
        return 'Fill in your distance';
      else
        return null;
    }

    String validateDuration(String value) {
      if (value.length == 0)
        return 'Fill in your duration';
      else
        return null;
    }

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Colors.lightBlue[800],
          leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                moveToLastScreen();
              }),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: _validateInputs,
              //color: Colors.lightBlueAccent,
            ),
          ],
        ),
        body: ListView(
          controller: controller,
          children: <Widget>[
            Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Fill in your plans below: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 30.0,),
                        Row(
                          children: <Widget>[
                            Icon(MaterialCommunityIcons.weight_kilogram,
                              size: 18
                            ),
                            Text(
                              ' Body Weight',
                              style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18.0
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                TextFormField(
                                  obscureText: false,
                                  style: new TextStyle(fontSize: 16.0),
                                  validator: validateTarget,
                                  onSaved: (String val) {
                                    _target = val;
                                  },
                                  decoration: InputDecoration(
                                    hintText: "Your target (in kg)",
                                    contentPadding: EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                                  ),
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                ),
                                SizedBox(height: 10.0),
                                TextFormField(
                                  obscureText: false,
                                  style: new TextStyle(fontSize: 16.0),
                                  validator: validateGl,
                                  onSaved: (String val) {
                                    _gl = val;
                                  },
                                  decoration: InputDecoration(
                                    hintText: "Daily gain/loss average (in kg/day with 2 decimal points)",
                                    contentPadding: EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                                  ),
                                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                                  inputFormatters: [BlacklistingTextInputFormatter(new RegExp('[\\-|\\ ]'))],
                                ),
                                SizedBox(height: 30.0),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(MaterialCommunityIcons.food_apple,
                              size: 18
                            ),
                            Text(
                              ' Calories and Food Nutrition',
                              style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18.0
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            TextFormField(
                              obscureText: false,
                              style: new TextStyle(fontSize: 16.0),
                              validator: validateCal,
                              onSaved: (String val) {
                                _cal = val;
                              },
                              decoration: InputDecoration(
                                hintText: "Calories (in kcal)",
                                contentPadding:
                                EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                              ),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                WhitelistingTextInputFormatter.digitsOnly
                              ],
                            ),
                            SizedBox(height: 10.0),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "* Percentage of 3 fields below must be 100% in total)",
                                style: TextStyle(fontSize: 12.0, color: AppTheme.deactivatedText),
                              ),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    margin: const EdgeInsets.only(right: 10, left: 10),
                                    child: TextFormField(
                                      obscureText: false,
                                      style: new TextStyle(fontSize: 16.0),
                                      validator: validateCal,
                                      onSaved: (String val) {
                                        _carbo = val;
                                      },
                                      decoration: InputDecoration(
                                        hintText: "Carbohydrate",
                                        contentPadding:
                                        EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                                      ),
                                      keyboardType: TextInputType.number,
                                      inputFormatters: <TextInputFormatter>[
                                        WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    margin: const EdgeInsets.only(right: 10, left: 10),
                                    child: TextFormField(
                                      obscureText: false,
                                      style: new TextStyle(fontSize: 16.0),
                                      validator: validateCal,
                                      onSaved: (String val) {
                                        _fat = val;
                                      },
                                      decoration: InputDecoration(
                                        hintText: "Fat",
                                        contentPadding:
                                        EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                                      ),
                                      keyboardType: TextInputType.number,
                                      inputFormatters: <TextInputFormatter>[
                                        WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    margin: const EdgeInsets.only(right: 10, left: 10),
                                    child: TextFormField(
                                      obscureText: false,
                                      style: new TextStyle(fontSize: 16.0),
                                      validator: validateCal,
                                      onSaved: (String val) {
                                        _protein = val;
                                      },
                                      decoration: InputDecoration(
                                        hintText: "Protein",
                                        contentPadding:
                                        EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                                      ),
                                      keyboardType: TextInputType.number,
                                      inputFormatters: <TextInputFormatter>[
                                        WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 30.0),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(Icons.local_drink,
                              size: 18
                            ),
                            Text(
                              ' Water Consumption',
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                            SizedBox(height: 30.0),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            TextFormField(
                              obscureText: false,
                              style: new TextStyle(fontSize: 16.0),
                              validator: validateDrink,
                              onSaved: (String val) {
                                _drink = val;
                              },
                              decoration: InputDecoration(
                                hintText: "Water consumed volume (in ml)",
                                contentPadding:
                                EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                              ),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                WhitelistingTextInputFormatter
                                    .digitsOnly
                              ],
                            ),
                            SizedBox(height: 30.0),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(MaterialCommunityIcons.sleep,
                              size: 18
                            ),
                            Text(
                              ' Sleep Duration',
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            TextFormField(
                              obscureText: false,
                              style: new TextStyle(fontSize: 16.0),
                              validator: validateDays,
                              onSaved: (String val) {
                                _days = val;
                              },
                              decoration: InputDecoration(
                                hintText: "Nap time duration (in minutes)",
                                contentPadding: EdgeInsets.fromLTRB(
                                    0, 0, 20.0, 0),
                              ),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                WhitelistingTextInputFormatter
                                    .digitsOnly
                              ],
                            ),
                            SizedBox(height: 10.0),
                            TextFormField(
                              obscureText: false,
                              style: new TextStyle(fontSize: 16.0),
                              validator: validateNights,
                              onSaved: (String val) {
                                _nights = val;
                              },
                              decoration: InputDecoration(
                                hintText: "Sleep time duration (in minutes)",
                                contentPadding: EdgeInsets.fromLTRB(
                                    0, 0, 20.0, 0),
                              ),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                WhitelistingTextInputFormatter
                                    .digitsOnly
                              ],
                            ),
                            SizedBox(height: 30.0),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(MaterialCommunityIcons.run_fast,
                              size: 18
                            ),
                            Text(
                              ' Workout',
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            TextFormField(
                              obscureText: false,
                              style: new TextStyle(fontSize: 16.0),
                              validator: validateDistance,
                              onSaved: (String val) {
                                _distance = val;
                              },
                              decoration: InputDecoration(
                                hintText: "Movement distance (in km with 1 decimal place)",
                                contentPadding: EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                              ),
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              inputFormatters: [BlacklistingTextInputFormatter(new RegExp('[\\-|\\ ]'))],
                            ),
                            SizedBox(height: 10.0),
                            TextFormField(
                              obscureText: false,
                              style: TextStyle(fontSize: 16.0),
                              validator: validateDuration,
                              onSaved: (String val) {
                                _duration = val;
                              },
                              decoration: InputDecoration(
                                hintText: "Duration (in minutes)",
                                contentPadding: EdgeInsets.fromLTRB(0, 0, 20.0, 0),
                              ),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[WhitelistingTextInputFormatter.digitsOnly],
                            ),
                            SizedBox(height: 10.0),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(30.0),
            title: Text('Are you sure you want to go back?'),
            content: Text('You will lose the data that you have input.'),
            actions: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Text(
                      "NO",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(width: 30.0),
                  GestureDetector(
                    onTap: () {
                      moveToLastScreen();
                    },
                    child: Text(
                      "YES",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                    height: 30.0,
                  ),
                ],
              ),
            ],
          ),
        ) ??
        false;
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
