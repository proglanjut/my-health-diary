import "package:flutter/material.dart";
import 'package:myhealthdiary/appTheme.dart';

import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/register/AccountCreated.dart';
import 'package:myhealthdiary/register/RegisterActivityPage.dart';
import 'package:myhealthdiary/register/RegisterChecklist.dart';
import 'package:myhealthdiary/register/RegisterPlansPage.dart';
import 'package:myhealthdiary/register/RegisterYouPage.dart';

import 'RegisterReview.dart';

class GoalPage extends StatefulWidget {
  String get title => "Goal";

  @override
  _GoalPageState createState() => _GoalPageState();
}

enum Goal { lose, maintain, gain }
Goal _goal = Goal.lose;

class _GoalPageState extends State<GoalPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RegisterPlans()),
                );
              },
            ),
          ],
          backgroundColor: Colors.lightBlue[800],
        ),

        body: ListView(
          controller: controller,
          children: <Widget>[
            Container(
              color: Colors.white,
              child: Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(36.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "What is your goal?",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20.0),
                          ),
                        ),
                        SizedBox(height: 15.0,),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                new Radio(
                                  value: Goal.lose,
                                  groupValue: _goal,
                                  onChanged: (Goal value) {
                                    setState(() {
                                      _goal = value;
                                    });
                                  },
                                ),
                                new Text(
                                  'Lose weight',
                                  style: new TextStyle(fontSize: 17.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                new Radio(
                                  value: Goal.maintain,
                                  groupValue: _goal,
                                  onChanged: (Goal value) {
                                    setState(() {
                                      _goal = value;
                                    });
                                  },
                                ),
                                new Text(
                                  'Maintain weight',
                                  style: new TextStyle(
                                    fontSize: 17.0,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                new Radio(
                                  value: Goal.gain,
                                  groupValue: _goal,
                                  onChanged: (Goal value) {
                                    setState(() {
                                      _goal = value;
                                    });
                                  },
                                ),
                                new Text(
                                  'Gain weight',
                                  style: new TextStyle(
                                    fontSize: 17.0,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
