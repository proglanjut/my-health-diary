import "package:flutter/material.dart";
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/database/DummyUser.dart';

import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:myhealthdiary/register/AccountCreated.dart';
import 'package:myhealthdiary/register/RegisterYouPage.dart';
import 'package:myhealthdiary/register/RegisterGoalPage.dart';

class ActivityPage extends StatefulWidget {
  String get title => "Activity";

  @override
  _ActivityPageState createState() => _ActivityPageState();
}

enum Activity { notact, lightly, active, veryact }
Activity _activity = Activity.notact;

class _ActivityPageState extends State<ActivityPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  Widget build(BuildContext context) {
    String userActivity = 'Not Very Active';
    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: () {
                DummyUser.loggedUser['activity'] = userActivity;
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => GoalPage()),
                );
              },
            ),
          ],
          backgroundColor: Colors.lightBlue[800],
        ),
        body: ListView(
          controller: controller,
          children: <Widget>[
            Container(
              color: Colors.white,
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "How active are you?",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20.0),
                        ),
                      ),
                      SizedBox(height: 15.0),
                      Align(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: Activity.notact,
                                  groupValue: _activity,
                                  onChanged: (Activity value) {
                                    setState(() {
                                      _activity = value;
                                      userActivity = 'Not Very Active';
                                    });
                                  },
                                ),
                                Expanded(
                                  child: ListTile(
                                    title: Text(
                                      "Not Very Active",
                                      style: TextStyle(fontSize: 17.0),
                                    ),
                                    subtitle: Text(
                                      "Spend most of the day sitting (e.g. bank teller, desk job)",
                                      style: TextStyle(fontSize: 15.0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 15.0),
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: Activity.lightly,
                                  groupValue: _activity,
                                  onChanged: (Activity value) {
                                    setState(() {
                                      _activity = value;
                                      userActivity = 'Lightly Active';
                                    });
                                  },
                                ),
                                Expanded(
                                  child: ListTile(
                                    title: Text(
                                      "Lightly Active",
                                      style: TextStyle(fontSize: 17.0),
                                    ),
                                    subtitle: Text(
                                      "Spend a good part of the day on your feet (e.g. teacher, salesperson)",
                                      style: TextStyle(fontSize: 15.0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 15.0),
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: Activity.active,
                                  groupValue: _activity,
                                  onChanged: (Activity value) {
                                    setState(() {
                                      _activity = value;
                                      userActivity = 'Active';
                                    });
                                  },
                                ),
                                Expanded(
                                  child: ListTile(
                                    title: Text(
                                      "Active",
                                      style: TextStyle(fontSize: 17.0),
                                    ),
                                    subtitle: Text(
                                      "Spend a good part of the day doing some physical activity (e.g. food server, postal carrier)",
                                      style: TextStyle(fontSize: 15.0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 15.0),
                            Row(
                              children: <Widget>[
                                Radio(
                                  value: Activity.veryact,
                                  groupValue: _activity,
                                  onChanged: (Activity value) {
                                    setState(() {
                                      _activity = value;
                                      userActivity = 'Very Active';
                                    });
                                  },
                                ),
                                Expanded(
                                  child: ListTile(
                                    title: Text(
                                      "Very Active",
                                      style: TextStyle(fontSize: 17.0),
                                    ),
                                    subtitle: Text(
                                      "Spend most of the day doing heavy physical activity (e.g. bike messenger, carpenter)",
                                      style: TextStyle(fontSize: 15.0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
