import "package:flutter/material.dart";
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/database/DummyUser.dart';

import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:myhealthdiary/register/RegisterYouPage.dart';
import 'package:myhealthdiary/register/RegisterGoalPage.dart';

class AccountCreated extends StatefulWidget {
  String get title => "Account Created";

  @override
  _AccountCreatedState createState() => _AccountCreatedState();
}

bool keepMeVal = true;
bool sendMeVal = true;

class _AccountCreatedState extends State<AccountCreated> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  Widget build(BuildContext context) {
    final startNowButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: SizedBox(
        width: 40.0,
        height: 48.0,
        child: Ink(
          decoration: const BoxDecoration(
            color: AppTheme.blue,
          ),
          child: MaterialButton(
            onPressed: () {
              setState(() {
                print(DummyUser.userData.length);
                print(DummyUser.userData.last.toString());
                DummyUser.userData.add(DummyUser.loggedUser);
                // DummyUser.userData.last.addAll(DummyUser.loggedUser);
                print(DummyUser.userData.length);
                print(DummyUser.userData.last.toString());
              });
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomePage(2)),
                    (Route<dynamic> route) => false,
              );
            },
            child: Text('Start Tracking Now', style: TextStyle(fontSize: 22.0, color: Colors.white)),
          ),
        ),
      ),
    );

    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Colors.lightBlue[800],
        ),
        body: ListView(
          shrinkWrap: true,
          controller: controller,
          padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 20.0),
          children: <Widget>[
            Container(
              color: Colors.white,
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.only(top: 36.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Congratulations!",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20.0),
                        ),
                      ),
                      SizedBox(height: 15.0),
                      Text(
                        'Your custom plan is ready and you are one step closer to your goal weight.',
                      ),
                      SizedBox(height: 15.0),
                      Text('Your daily goal is:'),
                      SizedBox(height: 15.0),
                      Text(
                        '${DummyUser.loggedUser['plans']['nutrition']['calories']}', //this may change according to user's input but we used this as an example
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.lightGreen,
                            fontSize: 50.0),
                      ),
                      SizedBox(height: 15.0),
                      Text('Calories'),
                      SizedBox(height: 15.0),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Checkbox(
                                value: keepMeVal,
                                onChanged: (bool value) {
                                  setState(() {
                                    keepMeVal = value;
                                  });
                                },
                              ),
                              Expanded(
                                child: ListTile(
                                  title: Text("Keep me on track with reminders."),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 15.0,),
                          Row(
                            children: <Widget>[
                              Checkbox(
                                value: sendMeVal,
                                onChanged: (bool value) {
                                  setState(() {
                                    sendMeVal = value;
                                  });
                                },
                              ),
                              Expanded(
                                child: ListTile(
                                  title: Text(
                                      "Send me the latest news, innovations, and offers from My Health Diary."),
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 100.0),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            startNowButton,
          ],
        ),

      ),
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
