import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/register/AccountCreated.dart';
import 'package:intl/intl.dart';
import '../appTheme.dart';

class RegisterReview extends StatefulWidget {
  String get title => "Review Your Account";

  @override
  _RegisterReviewPage createState() => _RegisterReviewPage();
}

class _RegisterReviewPage extends State<RegisterReview> {
  DateFormat dateFormat = DateFormat('yMMMMd'); //Output example: May 1, 2020
  
  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  @override
  Widget build(BuildContext context) {
    ScrollController controller;

    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AccountCreated()),
                );
              },
            ),
          ],
          backgroundColor: Colors.lightBlue[800],
        ),
        body: ListView(
          controller: controller,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          // To make the card compact
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(Icons.person_pin,
                                  color: AppTheme.blueTheme,
                                ),
                                Text(
                                  ' Personal Information',
                                  style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: AppTheme.blueTheme,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 15.0),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Username', style: TextStyle(fontSize: 14.0),),
                                    Text(DummyUser.loggedUser['username'], style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Email', style: TextStyle(fontSize: 14.0),),
                                    Text(DummyUser.loggedUser['email'], style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    Text('Birthdate',
                                      style: TextStyle(fontSize: 14.0),),
                                    Text(
                                      dateFormat.format(DateTime.parse(DummyUser.loggedUser['birthday'])), style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    Text('Gender',
                                      style: TextStyle(fontSize: 14.0),),
                                    Text(
                                      DummyUser.loggedUser['gender'], style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(Icons.local_hospital,
                                  color: AppTheme.blueTheme,
                                ),
                                Text(
                                  ' Health Information',
                                  style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: AppTheme.blueTheme,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Weight', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['weight']}  kg', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Height', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['height']} cm', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Body activity',
                                      style: TextStyle(fontSize: 14.0),),
                                    Text(DummyUser.loggedUser['activity'], style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(MaterialCommunityIcons.target,
                        color: AppTheme.deactivatedText,
                        size: 15,
                      ),
                      Text(' Your Plans ',
                          style: TextStyle(
                              color: AppTheme.deactivatedText
                          ),
                        ),
                      Expanded(
                        child: Divider(color: AppTheme.deactivatedText,),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(MaterialCommunityIcons.weight_kilogram,
                                  color: AppTheme.blueTheme,
                                ),
                                Text(
                                  ' Body Weight',
                                  style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: AppTheme.blueTheme,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Target body weight', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['plans']['bodyWeight']['target']} kg', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Average gain/loss per day', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['plans']['bodyWeight']['average']} kg/day', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Body activity',
                                      style: TextStyle(fontSize: 14.0),),
                                    Text(DummyUser.loggedUser['activity'], style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(MaterialCommunityIcons.food_apple,
                                  color: AppTheme.blueTheme,
                                ),
                                Text(
                                  ' Calories and Food Nutrition',
                                  style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: AppTheme.blueTheme,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Target Daily Calories', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['plans']['nutrition']['calories']} kcal', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Food Nutrition Percentage', style: TextStyle(fontSize: 14.0),),
                                        Text('(Carbohydrate/Fat/Protein)', style: TextStyle(fontSize: 14.0),),
                                      ],
                                    ),
                                    Text('${DummyUser.loggedUser['plans']['nutrition']['carbo']}%\/${DummyUser.loggedUser['plans']['nutrition']['fat']}%\/${DummyUser.loggedUser['plans']['nutrition']['protein']}%', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(MaterialCommunityIcons.glass_cocktail,
                                  color: AppTheme.blueTheme,
                                ),
                                Text(
                                  ' Water Consumption',
                                  style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: AppTheme.blueTheme,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Volume', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['plans']['water']['volume']} ml', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(MaterialCommunityIcons.sleep,
                                  color: AppTheme.blueTheme,
                                ),
                                Text(
                                  ' Sleep Duration',
                                  style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: AppTheme.blueTheme,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Nap time', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['plans']['sleep']['day']} minutes', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Sleep time', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['plans']['sleep']['night']} minutes', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(MaterialCommunityIcons.run_fast,
                                  color: AppTheme.blueTheme,
                                ),
                                Text(
                                  ' Workout',
                                  style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: AppTheme.blueTheme,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Movement distance', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['plans']['workout']['distance']} km', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Duration', style: TextStyle(fontSize: 14.0),),
                                    Text('${DummyUser.loggedUser['plans']['workout']['duration']} minutes', style: TextStyle(fontSize: 14.0),)
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}