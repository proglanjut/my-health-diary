import 'package:flutter/material.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/myPlansEnv/ChecklistItems.dart';
import 'package:myhealthdiary/myPlansEnv/DailyChecklistSettings.dart';
import 'package:intl/intl.dart';
import 'package:myhealthdiary/register/RegisterReview.dart';

import '../appTheme.dart';
import 'AccountCreated.dart';

class RegisterChecklist extends StatefulWidget {
  String get title => "Checklist Target";

  @override
  _RegisterAddChecklistPage createState() => _RegisterAddChecklistPage();
}

class _RegisterAddChecklistPage extends State<RegisterChecklist> {
  ScrollController controller;
  TabController tabController;
  bool isChanged = false;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  List<Map<String, dynamic>> selectedChecklist = DummyUser.loggedUser['checklist']['onList'];

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  Widget getTitle(data) {
    return Text(
      '${data['title']}',
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
    );
  }

  Widget getDesc(data) {
    return Text(
      '${data['description']}',
    );
  }

  Widget getSwitch(data) {
    return Ink(
      decoration: const ShapeDecoration(
        color: AppTheme.blueTheme,
        shape: CircleBorder(),
      ),
      child: IconButton(
        onPressed: () {
          setState(() {
            bool alreadyInPlan = false;
            DummyUser.loggedUser['checklist']['onList'].forEach((
                checklistInPlan) {
              if (checklistInPlan['id'] == data['id']) alreadyInPlan = true;
            });
            if (!alreadyInPlan) {
              data['used'] = 1;
              data['completion'] = 1;
              data['finished'] = false;
              DummyUser.loggedUser['checklist']['onList'].add(data);
              scaffoldKey.currentState.showSnackBar(
                  SnackBar(content: Text(
                      "\"${data['title']}\" was added to your plans!")));
            } else {
              scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    content: Text(
                        "\"${data['title']}\" already in your plans!"),
                    backgroundColor: Colors.red,
                  )
              );
            }
          });
        },
        color: Colors.white,
        icon: Icon(Icons.add),
      )
    );
  }

  Widget getCategory(data) {
    IconData whatIcon;
    String whatCategory;
    switch (data['category']) {
      case "food":
        whatIcon = Icons.local_dining;
        whatCategory = " Food and Drink";
        break;
      case "motion":
        whatIcon = Icons.accessibility_new;
        whatCategory = " Body Motions";
        break;
      case "sleep":
        whatIcon = Icons.local_hotel;
        whatCategory = " Sleep";
        break;
      case "habits":
        whatIcon = Icons.airline_seat_recline_extra;
        whatCategory = " Healthy Habits";
        break;
    }
    return Row(
      children: <Widget>[
        Icon(
          whatIcon,
          size: 11,
          color: AppTheme.blueTheme,
        ),
        Text(
          whatCategory,
          style: TextStyle(color: AppTheme.blueTheme, fontSize: 11),
        )
      ],
    );
  }

  Widget willWeShowTheList(Map<String, dynamic> selectedChecklists){
    int count = selectedChecklist.length;
    if(count == 0){
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(":'(",
                style: TextStyle(
                    color: AppTheme.blueTheme,
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold
                )
            ),
            SizedBox(height: 20,),
            Text("You don't have any checklist items. Set them now!",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            ),
            SizedBox(height: 10,),
            Text("Or skip this page and set them later :)",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            )
          ],
        ),
      );
    }else if(count == 1 && selectedChecklist[0]['id'] == 0){
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(":'(",
                style: TextStyle(
                    color: AppTheme.blueTheme,
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold
                )
            ),
            SizedBox(height: 20,),
            Text("You didn't have any checklist items. Set them now!",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            ),
            SizedBox(height: 10,),
            Text("But you can skip this page and set them later :)",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            )
          ],
        ),
      );
    }else{
      return Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Divider(
                    color: AppTheme.grey,
                  ),
                ),
                Text(
                  'Your Checklist Target. Swipe card to delete from your plans',
                  style: TextStyle(color: AppTheme.grey, fontSize: 12),
                ),
                Expanded(
                  child: Divider(
                    color: AppTheme.grey,
                  ),
                ),
              ],
            ),
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: selectedChecklists['onList'].length,
              physics: ClampingScrollPhysics(),
              itemBuilder: (context, index) {
                var item = selectedChecklists['onList'][index];
                return Dismissible(
                  key: Key(item['title']),
                  onDismissed: (direction) {
                    isChanged = true;
                    Scaffold.of(context).showSnackBar(
                        SnackBar(content: Text("\"${item['title']}\" was removed from your plans")));
                    setState(() {
                      selectedChecklists['onList'].removeAt(index);
                      DateTime date = DateTime.now();
                      selectedChecklists['lastChange'] = date.toString();
                    });
                  },
                  // Show a red background as the item is swiped away.
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    getCategory(item),
                                    getTitle(item),
                                    getDesc(item),
                                  ],
                                ),
                              ),

                            ],
                          )),
                    ),
                  ),
                );
              },
            ),
          ],
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if(selectedChecklist.length == 1 && selectedChecklist[0]['id'] == 0){
      DummyUser.loggedUser['checklist']['onList'].clear();
    }
    DateTime date = DateTime.now();
    DummyUser.loggedUser['checklist']['lastChange'] = date.toString();


    var food = ChecklistItems.foodChecklist,
        motion = ChecklistItems.motionChecklist,
        sleep = ChecklistItems.sleepChecklist,
        habit = ChecklistItems.habitChecklist;

    // TODO: implement build
    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },
      child: DefaultTabController(
        length: 5,
        child: Scaffold(
            key: scaffoldKey,
            resizeToAvoidBottomInset: false,
            backgroundColor: AppTheme.notWhite,
            appBar: AppBar(
              title: Text(widget.title),
              backgroundColor: AppTheme.blueTheme,
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RegisterReview()),
                    );
                  },
                ),
              ],
              bottom: TabBar(
                indicatorColor: Colors.lightBlueAccent,
                labelColor: Colors.lightBlueAccent,
                unselectedLabelColor: AppTheme.notWhite,
                tabs: <Widget>[
                  Tab(
                    icon: Icon(Icons.class_),
                    text: "My",
                  ),
                  Tab(
                    icon: Icon(Icons.local_dining),
                    text: "Food",
                  ),
                  Tab(
                    icon: Icon(Icons.accessibility_new),
                    text: "Motion",
                  ),
                  Tab(
                    icon: Icon(Icons.local_hotel),
                    text: "Sleep",
                  ),
                  Tab(
                    icon: Icon(Icons.airline_seat_recline_extra),
                    text: "Habits",
                  ),
                ],
              ),
            ),
            body: TabBarView(
              children: <Widget>[
                willWeShowTheList(DummyUser.loggedUser['checklist']),
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  controller: controller,
                  padding: EdgeInsets.all(10.0),
                  itemCount: food.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      getCategory(food[index]),
                                      getTitle(food[index]),
                                      getDesc(food[index]),
                                    ],
                                  )),
                                  getSwitch(food[index])
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  controller: controller,
                  padding: EdgeInsets.all(10.0),
                  itemCount: motion.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      getCategory(motion[index]),
                                      getTitle(motion[index]),
                                      getDesc(motion[index]),
                                    ],
                                  )),
                                  getSwitch(motion[index])
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  controller: controller,
                  padding: EdgeInsets.all(10.0),
                  itemCount: sleep.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      getCategory(sleep[index]),
                                      getTitle(sleep[index]),
                                      getDesc(sleep[index]),
                                    ],
                                  )),
                                  getSwitch(sleep[index])
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  controller: controller,
                  padding: EdgeInsets.all(10.0),
                  itemCount: habit.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      getCategory(habit[index]),
                                      getTitle(habit[index]),
                                      getDesc(habit[index]),
                                    ],
                                  )),
                                  getSwitch(habit[index])
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ],
            )),
      ),
    );
  }
}
