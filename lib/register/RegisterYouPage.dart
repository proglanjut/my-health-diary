import "package:flutter/material.dart";
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:myhealthdiary/appTheme.dart';

import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/register/RegisterActivityPage.dart';
import 'package:myhealthdiary/register/RegisterGoalPage.dart';

class SignUpYou extends StatefulWidget {
  String get title => "You";

  @override
  _SignUpYouState createState() => _SignUpYouState();
}

enum Gender { male, female }
Gender _gender = Gender.male;

class _SignUpYouState extends State<SignUpYou> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;

  bool _autoValidate = false;
  String _birthdate, bday;
  String _height, genderPick = 'Male';
  String _weight;

  DateTime selectedDate = DateTime.now();
  TextEditingController dateController = TextEditingController();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1970),
        lastDate: DateTime(2100));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        bday = picked.toString();
        dateController.text = selectedDate.year.toString() +
            '/ ' +
            selectedDate.month.toString() +
            '/ ' +
            selectedDate.day.toString();
      });
  }

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      Map<String, dynamic> temporaryUser = DummyUser.loggedUser;
      temporaryUser['birthday'] = bday;
      temporaryUser['height'] = int.parse(_height);
      temporaryUser['weight'] = int.parse(_weight);
      temporaryUser['gender'] = genderPick;
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return ActivityPage();
          },
        ),
      );
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget build(BuildContext context) {
    String validateBirthdate(String value) {
      if (value.length == 0)
        return 'Fill in your birthdate';
      else
        return null;
    }

    String validateHeight(String value) {
      if (value.length == 0)
        return 'Fill in your height';
      else
        return null;
    }

    String validateWeight(String value) {
      if (value.length == 0)
        return 'Fill in your weight';
      else
        return null;
    }

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Colors.lightBlue[800],
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: _validateInputs,
                //color: Colors.lightBlueAccent,
            ),
          ],
        ),
        body: ListView(
          controller: controller,
          children: <Widget>[
            Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Gender",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(height: 15.0,),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Radio(
                              value: Gender.male,
                              groupValue: _gender,
                              onChanged: (Gender value) {
                                setState(() {
                                  _gender = value;
                                  genderPick = 'Male';
                                });
                              },
                            ),
                            new Text(
                              'Male',
                              style: new TextStyle(fontSize: 17.0),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            new Radio(
                              value: Gender.female,
                              groupValue: _gender,
                              onChanged: (Gender value) {
                                setState(() {
                                  _gender = value;
                                  genderPick = 'Female';
                                });
                              },
                            ),
                            new Text(
                              'Female',
                              style: new TextStyle(
                                fontSize: 17.0,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Birthdate",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 25,
                      child: TextFormField(
                        onTap: () => _selectDate(context),
                        controller: dateController,
                        decoration: const InputDecoration(
                          suffix: Icon(
                            Icons.calendar_today,
                          ),
                          hintText: 'YYYY/ MM/ DD',
                        ),
                        validator: validateBirthdate,
                        onSaved: (String val) {
                          _birthdate = val;
                        },
                      ),
                    ),
                    SizedBox(height: 25.0),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Height",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    TextFormField(
                        obscureText: false,
                        style: new TextStyle(fontSize: 17.0),
                        validator: validateHeight,
                        onSaved: (String val) {
                          _height = val;
                        },
                        decoration: InputDecoration(
                          hintText: "cm",
                          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
                        ),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                    ),
                    SizedBox(height: 25.0),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Weight",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    TextFormField(
                        obscureText: false,
                        style: new TextStyle(fontSize: 17.0),
                        validator: validateWeight,
                        onSaved: (String val) {
                          _weight = val;
                        },
                        decoration: InputDecoration(
                          hintText: "kg",
                          contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
                        ),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                    ),
                    SizedBox(height: 50.0),
//                    Ink(
//                      decoration: const BoxDecoration(
//                        color: AppTheme.blue,
//                        borderRadius: BorderRadius.all(
//                          Radius.circular(5.0),
//                        ),
//                      ),
//                      child: MaterialButton(
//                        minWidth: double.infinity,
//                        height: 48.0,
//                        onPressed: _validateInputs,
//                        //color: Colors.lightBlueAccent,
//                        child: Text('Next', style: TextStyle(fontSize: 22.0, color: Colors.white)),
//                      ),
//                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        contentPadding: EdgeInsets.all(30.0),
        title: Text('Are you sure you want to go back?'),
        content: Text('You will lose the data that you have input.'),
        actions: <Widget>[
          Row(
            children: <Widget>[
              GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: Text(
                  "NO",
                  style: TextStyle(
                    color: AppTheme.blue,
                  ),
                ),
              ),
              SizedBox(width: 30.0),
              GestureDetector(
              onTap: () { moveToLastScreen();},
                child: Text(
                  "YES",
                  style: TextStyle(
                    color: AppTheme.blue,
                  ),
                ),
              ),
              SizedBox(width: 10.0, height: 30.0,),
            ],
          ),
        ],
      ),
    ) ?? false;
  }
  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
