import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myhealthdiary/myDiaryEnv/AddFood.dart';
import 'package:myhealthdiary/myDiaryEnv/AddSleep.dart';
import 'package:myhealthdiary/myDiaryEnv/AddWater.dart';
import 'package:myhealthdiary/myDiaryEnv/AddWorkout.dart';
import 'package:myhealthdiary/myPlansEnv/DailyChecklistSettings.dart';
import 'package:myhealthdiary/register/RegisterActivityPage.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';

import 'MyEmptyDiary.dart';
import 'Settings.dart';

class MyDiary extends StatefulWidget {
  String get title => "Diary";

  @override
  State<StatefulWidget> createState() {
    return _MyDiaryState();
  }
}

class _MyDiaryState extends State<MyDiary> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;

  void goToAddFood (){
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return AddFood();
            }
        )
    );
  }

  void goToAddWorkout (){
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return AddWorkout();
            }
        )
    );
  }

  void goToAddWater (){
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return AddWater();
            }
        )
    );
  }

  void goToAddSleep (){
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return AddSleep();
            }
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.notWhite,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: Colors.lightBlue[800],
        ),
        body: ListView(
          shrinkWrap: true,
          controller: controller,
          children: <Widget>[
            StickyHeader(
              header: Container(
                color: AppTheme.white,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: AppTheme.deactivatedText,
                        ),
//                      onPressed: moveToLastScreen,
                      ),
                      Text('Monday, May 11, 2020'),
                      IconButton(
                        icon: Icon(
                          Icons.arrow_forward_ios,
                          color: AppTheme.deactivatedText,
                        ),
                        onPressed: () {
//                        Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                            builder: (context) {
//                              return MyEmptyDiary();
//                            },
//                          ),
//                        );
                        },
                      ),
                    ],
                  ),
                ),
              ),
              content: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Total Calories',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18.0),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                LinearPercentIndicator(
                                  width: MediaQuery.of(context).size.width - 130,
                                  lineHeight: 8.0,
                                  percent: 0.3,
                                  backgroundColor: AppTheme.progress,
                                  progressColor: Colors.red,
                                ),
                                Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 20.0,
                                    ),
                                    Text('360',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold)),
                                    Text('kcal',
                                        style: TextStyle(
                                          fontSize: 10.0,
                                        )),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Total Food Nutrition',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18.0),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircularPercentIndicator(
                                  radius: 60.0,
                                  lineWidth: 4.0,
                                  percent: 0.70,
                                  center: Text("70%"),
                                  progressColor: AppTheme.blue,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                                ),
                                CircularPercentIndicator(
                                  radius: 60.0,
                                  lineWidth: 4.0,
                                  percent: 0.05,
                                  center: new Text("5%"),
                                  progressColor: Colors.green,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                                ),
                                CircularPercentIndicator(
                                  radius: 60.0,
                                  lineWidth: 4.0,
                                  percent: 0.25,
                                  center: new Text("25%"),
                                  progressColor: Colors.red,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 15.0,
                                  height: 15.0,
                                  decoration: new BoxDecoration(
                                    color: AppTheme.blue,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                Text(
                                  ' Carbohydrates',
                                  style: TextStyle(fontSize: 13.0),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 15.0,
                                  height: 15.0,
                                  decoration: new BoxDecoration(
                                    color: Colors.green,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                Text(
                                  ' Protein',
                                  style: TextStyle(fontSize: 13.0),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 15.0,
                                  height: 15.0,
                                  decoration: new BoxDecoration(
                                    color: Colors.red,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                Text(
                                  ' Fat',
                                  style: TextStyle(fontSize: 13.0),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Breakfast (03.00 am - 10.00 am)',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                                Text(
                                  '+240 kcal',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black,
                              thickness: 2.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Rice',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '200 kcal',
                                      style: TextStyle(fontSize: 14.0),
                                    )
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Tempe',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '40 kcal',
                                      style: TextStyle(fontSize: 14.0),
                                    )
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    goToAddFood();
                                  },
                                  child: Container(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Icon(Icons.add_circle),
                                        Text(
                                          ' Add new',
                                          style: TextStyle(fontSize: 14.0),
                                        ),
                                      ],
                                    ),
                                  )
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Lunch (11.00 am - 06.00 pm)',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                                Text(
                                  '+310 kcal',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black,
                              thickness: 2.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Rice',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '200 kcal',
                                      style: TextStyle(fontSize: 14.0),
                                    )
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Meatballs',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '110 kcal',
                                      style: TextStyle(fontSize: 14.0),
                                    )
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                GestureDetector(
                                    onTap: () {
                                      goToAddFood();
                                    },
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.add_circle),
                                          Text(
                                            ' Add new',
                                            style: TextStyle(fontSize: 14.0),
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Dinner (07.00 pm - 02.00 am)',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                                Text(
                                  '+0 kcal',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black,
                              thickness: 2.0,
                            ),
                            Column(
                              children: <Widget>[
                                GestureDetector(
                                    onTap: () {
                                      goToAddFood();
                                    },
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.add_circle),
                                          Text(
                                            ' Add new',
                                            style: TextStyle(fontSize: 14.0),
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Snack',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                                Text(
                                  '+50 kcal',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black,
                              thickness: 2.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'French Fries',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '50 kcal',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Divider(
                              color: AppTheme.grey,
                            ),
                            Column(
                              children: <Widget>[
                                GestureDetector(
                                    onTap: () {
                                      goToAddFood();
                                    },
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.add_circle),
                                          Text(
                                            ' Add new',
                                            style: TextStyle(fontSize: 14.0),
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Drinks',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                                Text(
                                  '810 ml',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black,
                              thickness: 2.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Coffee',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '100 ml',
                                      style: TextStyle(fontSize: 14.0),
                                    )
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Mineral Water',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '710 ml',
                                      style: TextStyle(fontSize: 14.0),
                                    )
                                  ],
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                GestureDetector(
                                    onTap: () {
                                      goToAddWater();
                                    },
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.add_circle),
                                          Text(
                                            ' Add new',
                                            style: TextStyle(fontSize: 14.0),
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Workout',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                                Text(
                                  '-240 kcal in 15 min',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black,
                              thickness: 2.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Walking',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '200 kcal for 3 km in 10 min',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Divider(
                              color: AppTheme.grey,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Gymnastic',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '40 kcal in 5 min',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Divider(
                              color: AppTheme.grey,
                            ),
                            Column(
                              children: <Widget>[
                                GestureDetector(
                                    onTap: () {
                                      goToAddWorkout();
                                    },
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.add_circle),
                                          Text(
                                            ' Add new',
                                            style: TextStyle(fontSize: 14.0),
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Sleep',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                                Text(
                                  '30 min',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black,
                              thickness: 2.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Day (11.33 am - 12.03 pm)',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Text(
                                      '30 min',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Divider(
                              color: AppTheme.grey,
                            ),
                            Column(
                              children: <Widget>[
                                GestureDetector(
                                    onTap: () {
                                      goToAddSleep();
                                    },
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(Icons.add_circle),
                                          Text(
                                            ' Add new',
                                            style: TextStyle(fontSize: 14.0),
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 15.0),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Daily Checklist Progression',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                                Text(
                                  '3 completed',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black,
                              thickness: 2.0,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Wake up early',
                                  style: TextStyle(fontSize: 14.0),
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Text(
                                  'Morning sunshine',
                                  style: TextStyle(fontSize: 14.0),
                                ),
                                Divider(
                                  color: AppTheme.grey,
                                ),
                                Text(
                                  'Eat fruit',
                                  style: TextStyle(fontSize: 14.0),
                                ),
                              ],
                            ),
                            Divider(
                              color: AppTheme.grey,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      'See your checklist ',
                                      style: TextStyle(fontSize: 14.0),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.all(0.0),
                                      width: 30,
                                      child: IconButton(
                                        icon: Icon(Icons.arrow_forward_ios),
                                        onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) {
                                              return DailyChecklist();
                                            },
                                          ),
                                        );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(30.0),
            title: Text('Confirm Exit'),
            content: Text('Are you sure you want to exit the app?'),
            actions: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Text(
                      "NO",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(width: 30.0),
                  GestureDetector(
                    onTap: () => SystemNavigator.pop(),
                    child: Text(
                      "YES",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                    height: 30.0,
                  ),
                ],
              ),
            ],
          ),
        ) ??
        false;
  }
  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
