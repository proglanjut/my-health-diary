import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myhealthdiary/myDiaryEnv/AddFood.dart';
import 'package:myhealthdiary/myDiaryEnv/AddSleep.dart';
import 'package:myhealthdiary/myDiaryEnv/AddWater.dart';
import 'package:myhealthdiary/myDiaryEnv/AddWorkout.dart';
import 'package:myhealthdiary/myDiaryEnv/DailyChecklist.dart';
import 'package:myhealthdiary/myPlansEnv/ChecklistItems.dart';
import 'package:myhealthdiary/register/RegisterActivityPage.dart';
import 'package:percent_indicator/percent_indicator.dart';

import 'Settings.dart';

class MyHome extends StatefulWidget {
  String get title => "My Health Diary";

  @override
  State<StatefulWidget> createState() {
    return _MyHomeState();
  }
}

class _MyHomeState extends State<MyHome> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;
  int finishedItem = 0,
      totalItem = 0;

  void goToAddFood() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return AddFood();
            }
        )
    );
  }

  void goToAddWorkout() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return AddWorkout();
            }
        )
    );
  }

  void goToAddWater() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return AddWater();
            }
        )
    );
  }

  void goToAddSleep() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) {
              return AddSleep();
            }
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    var selectedChecklists = DummyUser.loggedUser['checklist']['onList'];
    totalItem = selectedChecklists.length;
    finishedItem = 0;
    selectedChecklists.forEach((item) {
      bool isFinished = item['finished'];
      if (isFinished) finishedItem++;
    });

    double percent = finishedItem / totalItem;

    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
        IconButton(
          icon: Icon(Icons.settings),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return Settings();
                },
              ),
            );
          },
        ),
      ],
    );

    Future<void> _addFoodAsk() async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))
            ),
            title: Text('Add Food'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('What kind of food that you want to add?.'),
                  FlatButton(
                    onPressed: () {
                      goToAddFood();
                    },
                    child: Text("Breakfast",
                      style: TextStyle(
                        color: AppTheme.white,
                      ),
                    ),
                    color: AppTheme.blueTheme,
                  ),
                  FlatButton(
                    onPressed: () {
                      goToAddFood();
                    },
                    child: Text("Lunch",
                      style: TextStyle(
                        color: AppTheme.white,
                      ),
                    ),
                    color: AppTheme.blueTheme,
                  ),
                  FlatButton(
                    onPressed: () {
                      goToAddFood();
                    },
                    child: Text("Dinner",
                      style: TextStyle(
                        color: AppTheme.white,
                      ),
                    ),
                    color: AppTheme.blueTheme,
                  ),
                  FlatButton(
                    onPressed: () {
                      goToAddFood();
                    },
                    child: Text("Snack",
                      style: TextStyle(
                        color: AppTheme.white,
                      ),
                    ),
                    color: AppTheme.blueTheme,
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancel',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    Widget anyChecklistItems() {
      if (totalItem != 0) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(Icons.check_box,
                  size: 16,
                ),
                Text(
                  'Today\'s Checklist',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            LinearPercentIndicator(
              width: MediaQuery
                  .of(context)
                  .size
                  .width - 230,
              lineHeight: 8.0,
              percent: percent,
              backgroundColor: AppTheme.progress,
              progressColor: (percent == 1)
                  ? Colors.green
                  : Colors.red,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              '$finishedItem of $totalItem completed',
              style: TextStyle(fontSize: 12.0),
            ),
          ],
        );
      } else {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(Icons.check_box,
                  size: 16,
                ),
                Text(
                  'Today\'s Checklist',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0),
                ),
              ],
            ),
            SizedBox(
              height: 5.0,
            ),
            Icon(MaterialCommunityIcons.delete_empty,
              color: AppTheme.blueTheme,
              size: 16,
            ),
            Text(
              'Your Checklist is Empty',
              style: TextStyle(fontSize: 13.0),
            ),
          ],
        );
      }
    }

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.notWhite,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: Colors.lightBlue[800],
        ),
        floatingActionButton: SpeedDial(
          backgroundColor: AppTheme.blue,
          animatedIcon: AnimatedIcons.add_event,
          animatedIconTheme: IconThemeData(
              size: 22.0
          ),
          visible: true,
          curve: Curves.bounceIn,
          children: [
            SpeedDialChild(
              child: Icon(
                Icons.local_hotel,
                color: AppTheme.blueTheme,
              ),
              backgroundColor: AppTheme.white,
              onTap: () {
                goToAddSleep();
              },
              label: "Add Sleep",
            ),
            SpeedDialChild(
              child: Icon(
                Icons.directions_run,
                color: AppTheme.blueTheme,
              ),
              backgroundColor: AppTheme.white,
              onTap: () {
                goToAddWorkout();
              },
              label: "Add Workout",
            ),
            SpeedDialChild(
              child: Icon(
                Icons.local_bar,
                color: AppTheme.blueTheme,
              ),
              backgroundColor: AppTheme.white,
              onTap: () {
                goToAddWater();
              },
              label: "Add Drink",
            ),
            SpeedDialChild(
              child: Icon(
                Icons.local_dining,
                color: AppTheme.blueTheme,
              ),
              backgroundColor: AppTheme.white,
              onTap: () {
                _addFoodAsk();
              },
              label: "Add Food",
            ),
          ],
        ),
        body: Stack(
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/images/logo-opacity.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
            ListView(
              shrinkWrap: true,
              controller: controller,
              padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(MaterialCommunityIcons.weight_kilogram,
                                size: 18,
                              ),
                              Text(
                                'Body Weight Progression',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18.0),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              LinearPercentIndicator(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width - 130,
                                lineHeight: 8.0,
                                percent: 0.3,
                                backgroundColor: AppTheme.progress,
                                progressColor: AppTheme.blue,
                              ),
                              Row(
                                children: <Widget>[
                                  SizedBox(
                                    width: 20.0,
                                  ),
                                  Text('+2',
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold)),
                                  Text('kg',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                      )),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            'From 60 kg to 65 kg',
                            style: TextStyle(fontSize: 13.0),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Average Body Gain/Loss',
                                style: TextStyle(fontSize: 15.0),
                              ),
                              Row(
                                children: <Widget>[
                                  Text('+0.25',
                                      style: TextStyle(
                                        fontSize: 15.0,
                                      )),
                                  Text('kg/day',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                      )),
                                ],
                              ),
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Estimated Day(s) Remaining',
                                style: TextStyle(fontSize: 15.0),
                              ),
                              Row(
                                children: <Widget>[
                                  Text('12',
                                      style: TextStyle(
                                        fontSize: 15.0,
                                      )),
                                  Text('days',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                      )),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: GridView.count(
                    primary: false,
                    crossAxisCount: 2,
                    childAspectRatio: 1.8,
                    mainAxisSpacing: 1.0,
                    crossAxisSpacing: 1.0,
                    children: <Widget>[
                      Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(Icons.accessibility_new,
                                    size: 16,
                                  ),
                                  Text(
                                    'My Health Rating',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      'B',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                    decoration: new BoxDecoration(
                                      borderRadius: new BorderRadius.all(
                                          new Radius.circular(10.0)),
                                      color: Colors.tealAccent,
                                    ),
                                    padding: new EdgeInsets.fromLTRB(
                                        10.0, 5.0, 10.0, 5.0),
                                  ),
                                  Text('75%',
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return TodayChecklist(); //UNCOMMENT WHEN PAGE IS DONE
                              },
                            ),
                          );
                        },
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: anyChecklistItems(),
                          ),
                        ),
                      ),

                    ],
                    //new Cards()
                    shrinkWrap: true,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Divider(
                          color: AppTheme.grey,
                        ),
                      ),
                      Text(
                        'Today\'s Diary Progression',
                        style: TextStyle(color: AppTheme.deactivatedText),
                      ),
                      Expanded(
                        child: Divider(
                          color: AppTheme.grey,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text(
                                'Food',
                                style: TextStyle(fontSize: 15.0),
                              ),
                              SizedBox(height: 10.0),
                              Icon(Icons.fastfood,
                                  size: 50, color: AppTheme.blue),
                              SizedBox(height: 10.0),
                              Text(
                                '600kcal',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0),
                              ),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                'Exercise',
                                style: TextStyle(fontSize: 15.0),
                              ),
                              SizedBox(height: 10.0),
                              Icon(Icons.accessibility_new,
                                  size: 50, color: AppTheme.blue),
                              SizedBox(height: 10.0),
                              Text(
                                '240kcal',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0),
                              ),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                'Target',
                                style: TextStyle(fontSize: 15.0),
                              ),
                              SizedBox(height: 10.0),
                              Icon(Icons.done_all,
                                  size: 50, color: AppTheme.blue),
                              SizedBox(height: 10.0),
                              Text(
                                '-640kcal',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0,
                                    color: Colors.red),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(MaterialCommunityIcons.sleep, size: 18),
                              Text(
                                'Sleep',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18.0),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              LinearPercentIndicator(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width - 130,
                                lineHeight: 8.0,
                                percent: 0.1,
                                backgroundColor: AppTheme.progress,
                                progressColor: Colors.red,
                              ),
                              Row(
                                children: <Widget>[
                                  Text('30',
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold)),
                                  Text('min',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                      )),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            'Sleep target: 450 minutes',
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(Icons.local_drink, size: 18,),
                              Text(
                                'Drink Consumption',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18.0),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              LinearPercentIndicator(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width - 130,
                                lineHeight: 8.0,
                                percent: 1,
                                backgroundColor: AppTheme.progress,
                                progressColor: Colors.green,
                              ),
                              Row(
                                children: <Widget>[
                                  Text('810',
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold)),
                                  Text('ml',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                      )),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            'Water target: 800 ml',
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30.0,),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(30.0),
            title: Text('Confirm Exit'),
            content: Text('Are you sure you want to exit the app?'),
            actions: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Text(
                      "NO",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(width: 30.0),
                  GestureDetector(
                    onTap: () => SystemNavigator.pop(),
                    child: Text(
                      "YES",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                    height: 30.0,
                  ),
                ],
              ),
            ],
          ),
    ) ??
        false;
  }
}
