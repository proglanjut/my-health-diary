import 'package:flutter/services.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myhealthdiary/register/RegisterActivityPage.dart';

import 'Settings.dart';

//THIS IS AN EXAMPLE, IF U HAVE ANY IDEA PLEASE EDIT

class Settings extends StatefulWidget {
  String get title => "Settings";

  @override
  State<StatefulWidget> createState() {
    return _SettingsState();
  }

}
class _SettingsState extends State<Settings>{
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;

  @override
  Widget build(BuildContext context) {
    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },

      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.white,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: Colors.lightBlue[800],
          leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                moveToLastScreen();
              }),
        ),
        body: ListView(
          shrinkWrap: true,
          controller: controller,
          padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              'Notifications'
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios),
                            onPressed: () {
//                              Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                  builder: (context) {
//                                    return EditNotification();     //UNCOMMENT WHEN PAGE IS DONE
//                                  },
//                                ),
//                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              'Article'
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios),
                            onPressed: () {
//                              Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                  builder: (context) {
//                                    return EditArticle();     //UNCOMMENT WHEN PAGE IS DONE
//                                  },
//                                ),
//                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              'Diary'
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios),
                            onPressed: () {
//                              Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                  builder: (context) {
//                                    return EditDiary();     //UNCOMMENT WHEN PAGE IS DONE
//                                  },
//                                ),
//                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              'Home'
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios),
                            onPressed: () {
//                              Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                  builder: (context) {
//                                    return EditHome();     //UNCOMMENT WHEN PAGE IS DONE
//                                  },
//                                ),
//                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              'Plans'
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios),
                            onPressed: () {
//                              Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                  builder: (context) {
//                                    return EditPlans();     //UNCOMMENT WHEN PAGE IS DONE
//                                  },
//                                ),
//                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              'Profile'
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios),
                            onPressed: () {
//                              Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                  builder: (context) {
//                                    return EditProfile();     //UNCOMMENT WHEN PAGE IS DONE
//                                  },
//                                ),
//                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              'Support and Rate Us'
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios),
                            onPressed: () {
//                              Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                  builder: (context) {
//                                    return SupportNRate();     //UNCOMMENT WHEN PAGE IS DONE
//                                  },
//                                ),
//                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }
}
