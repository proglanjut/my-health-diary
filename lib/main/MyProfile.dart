import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/main.dart';
import 'package:myhealthdiary/main/HomePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myhealthdiary/myEditEnv/EditProfile.dart';
import 'package:intl/intl.dart';
import 'package:myhealthdiary/register/RegisterActivityPage.dart';
import 'package:myhealthdiary/welcome/WelcomePage.dart';

import 'Settings.dart';

class MyProfile extends StatefulWidget {
  String get title => "Profile";

  @override
  State<StatefulWidget> createState() {
    return _MyProfileState();
  }

}

class _MyProfileState extends State<MyProfile> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;
  Map<String, dynamic> userInfo = DummyUser.loggedUser;

  @override
  Widget build(BuildContext context) {
    DateFormat format = DateFormat('yMMMMd'); //Output example: May 1, 2020
    DateTime date = DateTime.parse(userInfo['birthday']);
    String name = userInfo['name'],
        username = userInfo['username'],
        email = userInfo['email'],
        gender = userInfo['gender'],
        activity = userInfo['activity'],
        birthday = format.format(date);
    int height = userInfo['height'],
        weight = userInfo['weight'];


    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
        IconButton(
          icon: Icon(Icons.edit),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return EditProfile();
                },
              ),
            );
          },
        ),
      ],
    );

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.notWhite,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: Colors.lightBlue[800],
        ),
        body: ListView(
          shrinkWrap: true,
          controller: controller,
          padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 0,
                color: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        top: Consts.avatarRadius + Consts.padding,
                        bottom: Consts.padding,
                        left: Consts.padding,
                        right: Consts.padding,
                      ),
                      margin: EdgeInsets.only(top: Consts.avatarRadius),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(Consts.padding),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 10.0,
                            offset: const Offset(0.0, 5.0),
                          ),
                        ],
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        // To make the card compact
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            username,
                            style: TextStyle(
                              fontSize: 17.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Email', style: TextStyle(fontSize: 14.0),),
                                  Text(email, style: TextStyle(fontSize: 14.0),)
                                ],
                              ),
                              Divider(
                                color: AppTheme.grey,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: <Widget>[
                                  Text('Birthdate',
                                    style: TextStyle(fontSize: 14.0),),
                                  Text(
                                    birthday, style: TextStyle(fontSize: 14.0),)
                                ],
                              ),
                              Divider(
                                color: AppTheme.grey,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: <Widget>[
                                  Text('Gender',
                                    style: TextStyle(fontSize: 14.0),),
                                  Text(
                                    gender, style: TextStyle(fontSize: 14.0),)
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: Consts.padding,
                      right: 200.0,
                      child: CircleAvatar(
                        backgroundImage: AssetImage(
                            "assets/images/logo-circle.png"),
                        radius: Consts.avatarRadius,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(Icons.local_hospital,
                          ),
                          Text(
                            ' Health Information',
                            style: TextStyle(
                              fontSize: 17.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Weight', style: TextStyle(fontSize: 14.0),),
                              Text('$weight Kg', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Height', style: TextStyle(fontSize: 14.0),),
                              Text('$height Cm', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Body activity',
                                style: TextStyle(fontSize: 14.0),),
                              Text(activity, style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(MaterialCommunityIcons.bulletin_board,
                          ),
                          Text(
                            ' Your Account Summary',
                            style: TextStyle(
                              fontSize: 17.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Member since',
                                style: TextStyle(fontSize: 14.0),),
                              Text('May 1, 2020',
                                style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Average daily calories',
                                style: TextStyle(fontSize: 14.0),),
                              Text(
                                '823 kcal', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Average daily checklist items passed',
                                style: TextStyle(fontSize: 14.0),),
                              Text('3', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Average sleep duration',
                                style: TextStyle(fontSize: 14.0),),
                              Text('323 minutes',
                                style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Average water consumed',
                                style: TextStyle(fontSize: 14.0),),
                              Text('300 ml', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Average workout duration',
                                style: TextStyle(fontSize: 14.0),),
                              Text(
                                '10 minutes', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Total daily calories target passed',
                                style: TextStyle(fontSize: 14.0),),
                              Text('4', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Total daily checklist completed',
                                style: TextStyle(fontSize: 14.0),),
                              Text('2', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Total sleep duration target passed',
                                style: TextStyle(fontSize: 14.0),),
                              Text('3', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Total water consumption target passed',
                                style: TextStyle(fontSize: 14.0),),
                              Text('1', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Total workout duration target passed',
                                style: TextStyle(fontSize: 14.0),),
                              Text('6', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                          Divider(
                            color: AppTheme.grey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Total all daily target passed in a day',
                                style: TextStyle(fontSize: 14.0),),
                              Text('0', style: TextStyle(fontSize: 14.0),)
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                color: Colors.transparent,
                elevation: 0,
                child: RaisedButton(
                  elevation: 5,
                  onPressed: () => _logoutConfirmation(),
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Text('Log Out',
                      style: TextStyle(
                        color: Colors.white,
                      )
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }



  Future<bool> _logoutConfirmation() {
    return showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(30.0),
            title: Text('Confirm Logout'),
            content: Text('Are you sure you want to log out?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(
                  "NO",
                  style: TextStyle(
                    color: AppTheme.blue,
                  ),
                ),
              ),
              FlatButton(
                onPressed: () {
                  //Remove all data in loggedUser
                  //Debugging: Check if there is another data in loggedUser
                  print('loggedUser: ${DummyUser.loggedUser.length} \/\/It must be 0!');
                  //Back to welcome page
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => WelcomePage()),
                  );
                },
                child: Text(
                  "YES",
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              ),
            ],
          ),
    ) ?? false;
  }

  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(30.0),
            title: Text('Confirm Exit'),
            content: Text('Are you sure you want to exit the app?'),
            actions: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Text(
                      "NO",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(width: 30.0),
                  GestureDetector(
                    onTap: () => SystemNavigator.pop(),
                    child: Text(
                      "YES",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(width: 10.0, height: 30.0,),
                ],
              ),
            ],
          ),
    ) ?? false;
  }
}

class Consts {
  Consts._();

  static const double padding = 10.0;
  static const double avatarRadius = 50.0;
}
