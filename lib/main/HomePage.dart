import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:myhealthdiary/main/MyArticle.dart';

import 'MyDiary.dart';
import 'MyHome.dart';
import 'MyPlans.dart';
import 'MyProfile.dart';
import 'Settings.dart';

class HomePage extends StatefulWidget {
  String get title => "My Health Diary";
  int pageIndex;

  HomePage(this.pageIndex);

  @override
  State<StatefulWidget> createState() {
    return _HomePageState(pageIndex);
  }
}

class _HomePageState extends State<HomePage> {
  int _currentIndex;
  _HomePageState(this._currentIndex);
  final List<Widget> _children = [
    MyArticle(),
    MyDiary(),
    MyHome(),
    MyPlans(),
    MyProfile(),
  ];
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        contentPadding: EdgeInsets.all(30.0),
        title: Text('Confirm Exit'),
        content: Text('Are you sure you want to exit the app?'),
        actions: <Widget>[
          Row(
            children: <Widget>[
              GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: Text(
                  "NO",
                  style: TextStyle(
                    color: AppTheme.blue,
                  ),
                ),
              ),
              SizedBox(width: 30.0),
              GestureDetector(
                onTap: () => SystemNavigator.pop(),
                child: Text(
                  "YES",
                  style: TextStyle(
                    color: AppTheme.blue,
                  ),
                ),
              ),
              SizedBox(width: 10.0, height: 30.0,),
            ],
          ),
        ],
      ),
    ) ?? false;
  }
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: AppTheme.notWhite,
        body: _children[_currentIndex],
        bottomNavigationBar: CurvedNavigationBar(
          backgroundColor: Color(0xFFEDF0F2),
          color: Colors.lightBlue[800],
          buttonBackgroundColor: Colors.lightBlue[800],
          height: 60,
          animationDuration: Duration(
            milliseconds: 200,
          ),
          index: widget.pageIndex,
          animationCurve: Curves.bounceInOut,
          items: <Widget>[
            Icon(Icons.dashboard, size: 30, color: Colors.white),
            Icon(Icons.book, size: 30, color: Colors.white),
            Icon(Icons.home, size: 30, color: Colors.white),
            Icon(Icons.format_list_bulleted, size: 30, color: Colors.white),
            Icon(Icons.account_circle, size: 30, color: Colors.white),
          ],
          onTap: onTabTapped,
        ),
      ),
    );
  }
}
