import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/database/DummyUser.dart';
import 'package:myhealthdiary/myPlansEnv/BodyWeight.dart';
import 'package:myhealthdiary/myPlansEnv/DailyChecklistSettings.dart';
import 'package:myhealthdiary/myPlansEnv/Nutrition.dart';

class MyPlans extends StatefulWidget {
  String get title => "Plans";

  @override
  State<StatefulWidget> createState() {
    return _MyPlansState();
  }
}

class _MyPlansState extends State<MyPlans> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  ScrollController controller;
  Map<String, dynamic> userInfo = DummyUser.loggedUser;

  void goToBodyWeight() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return BodyWeight();
    }));
  }

  void goNutrition() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return Nutrition();
    }));
  }

  @override
  Widget build(BuildContext context) {
    //Body Weight
    int target = userInfo['plans']['bodyWeight']['target'];
    double average = userInfo['plans']['bodyWeight']['average'];
    //Calories and Food
    int calories = userInfo['plans']['nutrition']['calories'],
        carbo = userInfo['plans']['nutrition']['carbo'],
        fat = userInfo['plans']['nutrition']['fat'],
        protein = userInfo['plans']['nutrition']['protein'];
    //Sleep
    int day = userInfo['plans']['sleep']['day'],
        night = userInfo['plans']['sleep']['night'];
    //Drinks
    int volume = userInfo['plans']['water']['volume'];
    //Workout
    double distance = userInfo['plans']['workout']['distance'];
    int duration = userInfo['plans']['workout']['duration'];

    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.notWhite,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: Colors.lightBlue[800],
        ),
        body: ListView(
          shrinkWrap: true,
          controller: controller,
          padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.info_outline,
                    color: AppTheme.deactivatedText,
                    size: 13.0,
                  ),
                  Text(
                    ' Tap at card to change your target',
                    style: TextStyle(
                      color: AppTheme.deactivatedText,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: GestureDetector(
                  onTap: () {
                    goToBodyWeight();
                  },
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(MaterialCommunityIcons.weight_kilogram,
                                color: AppTheme.blueTheme,
                                size: 15,
                              ),
                              Text(
                                ' Body Weight',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  color: AppTheme.blueTheme,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              SizedBox(
                                width: 5.0,
                              ),
                              Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('$target',
                                          style: TextStyle(
                                              fontSize: 30.0,
                                              fontWeight: FontWeight.bold)),
                                      Text(' kg',
                                          style: TextStyle(
                                            fontSize: 13.0,
                                          )),
                                    ],
                                  ),
                                  Text(
                                    'Target',
                                    style: TextStyle(
                                      fontSize: 13.0,
                                    ),
                                  )
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('$average',
                                          style: TextStyle(
                                              fontSize: 30.0,
                                              fontWeight: FontWeight.bold)),
                                      Text(' kg/day',
                                          style: TextStyle(
                                            fontSize: 13.0,
                                          )),
                                    ],
                                  ),
                                  Text(
                                    'Gain/Loss Average',
                                    style: TextStyle(
                                      fontSize: 13.0,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          )
                        ],
                      ),
                    ),
                  ),
                )),
            Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: GestureDetector(
                  onTap: () {
                    goNutrition();
                  },
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(MaterialCommunityIcons.food_apple,
                                color: AppTheme.blueTheme,
                                size: 15,
                              ),
                              Text(
                                ' Daily Calories and Food Nutrition',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  color: AppTheme.blueTheme,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              SizedBox(
                                width: 3.0,
                              ),
                              Column(
                                children: <Widget>[
                                  Text(
                                    '$calories',
                                    style: TextStyle(
                                      fontSize: 30.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text('Calories',
                                      style: TextStyle(
                                        fontSize: 13.0,
                                      )),
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('$carbo',
                                          style: TextStyle(
                                              fontSize: 30.0,
                                              fontWeight: FontWeight.bold)),
                                      Text('%',
                                          style: TextStyle(
                                            fontSize: 13.0,
                                          )),
                                    ],
                                  ),
                                  Text(
                                    'Carbohydrate',
                                    style: TextStyle(
                                      fontSize: 13.0,
                                    ),
                                  )
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('$protein',
                                          style: TextStyle(
                                              fontSize: 30.0,
                                              fontWeight: FontWeight.bold)),
                                      Text('%',
                                          style: TextStyle(
                                            fontSize: 13.0,
                                          )),
                                    ],
                                  ),
                                  Text(
                                    'Protein',
                                    style: TextStyle(
                                      fontSize: 13.0,
                                    ),
                                  )
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('$fat',
                                          style: TextStyle(
                                              fontSize: 30.0,
                                              fontWeight: FontWeight.bold)),
                                      Text('%',
                                          style: TextStyle(
                                            fontSize: 13.0,
                                          )),
                                    ],
                                  ),
                                  Text(
                                    'Fat',
                                    style: TextStyle(
                                      fontSize: 13.0,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                width: 3.0,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          )
                        ],
                      ),
                    ),
                  ),
                )),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.local_drink,
                            color: AppTheme.blueTheme,
                            size: 15,
                          ),
                          Text(
                            ' Water Consumption',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: AppTheme.blueTheme,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('$volume',
                                      style: TextStyle(
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.bold)),
                                  Text(' ml',
                                      style: TextStyle(
                                        fontSize: 13.0,
                                      )),
                                ],
                              ),
                              Text(
                                'Total',
                                style: TextStyle(
                                  fontSize: 13.0,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(MaterialCommunityIcons.sleep,
                            color: AppTheme.blueTheme,
                            size: 15,
                          ),
                          Text(
                            ' Sleep Duration',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: AppTheme.blueTheme,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SizedBox(
                            width: 5.0,
                          ),
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('$day',
                                      style: TextStyle(
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.bold)),
                                  Text(' min',
                                      style: TextStyle(
                                        fontSize: 13.0,
                                      )),
                                ],
                              ),
                              Text(
                                'Day',
                                style: TextStyle(
                                  fontSize: 13.0,
                                ),
                              )
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('$night',
                                      style: TextStyle(
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.bold)),
                                  Text(' min',
                                      style: TextStyle(
                                        fontSize: 13.0,
                                      )),
                                ],
                              ),
                              Text(
                                'Night',
                                style: TextStyle(
                                  fontSize: 13.0,
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(MaterialCommunityIcons.run_fast,
                            color: AppTheme.blueTheme,
                            size: 15,
                          ),
                          Text(
                            ' Daily Workout',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: AppTheme.blueTheme,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SizedBox(
                            width: 5.0,
                          ),
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('$duration',
                                      style: TextStyle(
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.bold)),
                                  Text(' min',
                                      style: TextStyle(
                                        fontSize: 13.0,
                                      )),
                                ],
                              ),
                              Text(
                                'Duration',
                                style: TextStyle(
                                  fontSize: 13.0,
                                ),
                              )
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('$distance',
                                      style: TextStyle(
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.bold)),
                                  Text(' km',
                                      style: TextStyle(
                                        fontSize: 13.0,
                                      )),
                                ],
                              ),
                              Text(
                                'Distance',
                                style: TextStyle(
                                  fontSize: 13.0,
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text(
                            'Set Additional Checklist',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Text(
                            'Current checklist: 5 items',
                            style: TextStyle(fontSize: 14),
                          ),
                        ],
                      ),
                      IconButton(
                        icon: Icon(Icons.arrow_forward_ios),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return DailyChecklist(); //UNCOMMENT WHEN PAGE IS DONE
                              },
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(30.0),
            title: Text('Confirm Exit'),
            content: Text('Are you sure you want to exit the app?'),
            actions: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Text(
                      "NO",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(width: 30.0),
                  GestureDetector(
                    onTap: () => SystemNavigator.pop(),
                    child: Text(
                      "YES",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                    height: 30.0,
                  ),
                ],
              ),
            ],
          ),
        ) ??
        false;
  }
}
