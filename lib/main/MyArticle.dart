import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:myhealthdiary/appTheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myhealthdiary/myArticleEnv/Articles.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:sticky_headers/sticky_headers.dart';
import 'package:url_launcher/url_launcher.dart';


class MyArticle extends StatefulWidget {
  String get title => "Article";

  @override
  State<StatefulWidget> createState() {
    return _MyArticleState();
  }
}

class _MyArticleState extends State<MyArticle> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  Future<List<Articles>> futureArticle;
  ScrollController controller;
  TextEditingController editingController;
  List<Articles> items = List<Articles>(), articles = List<Articles>();

  //Method for get the articles
  Future<List<Articles>> fetchArticle() async {
    List<Articles> list;
    String link =
        "https://newsapi.org/v2/everything?excludeDomains=slickdeals.net&qInTitle=workout OR body fit OR healthy&language=en&pageSize=50&apiKey=976f59e4701140a497b5acd5b86a866e";
    var res = await http
        .get(Uri.encodeFull(link), headers: {"Accept": "application/json"});
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      var rest = data["articles"] as List;
      list = rest.map<Articles>((json) => Articles.fromJson(json)).toList();
    }
    //Debugging: Print list size in your run terminal
    print("List Size: ${list.length}");
    return list;
  }

  //Method untuk mencari artikel yang menganung string yang diinput user di article search bar
  void filterSearchResults(String query) {
    List<Articles> dummySearchList = List<Articles>();
    dummySearchList.addAll(articles);
    if (query.isNotEmpty) {
      print(query);
      List<Articles> dummyListData = List<Articles>();
      dummySearchList.forEach((item) {
        print("${item.title.toString()} and ${query.toLowerCase()}");
        print(item.title.toString().contains(query.toLowerCase()));
        if (item.title.toString().toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(articles);
      });
    }
  }

  //Method untuk memilih antara menampilkan list atau menampilkan pesan error
  //jika tidak ada artikel yang sesuai user input
  Widget willWeShowTheList(int count){
    if(count == 0){
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(":'(",
                style: TextStyle(
                    color: AppTheme.blueTheme,
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold
                )
            ),
            SizedBox(height: 20,),
            Text("Sorry we can't find articles that you want",
                style: TextStyle(
                  color: AppTheme.blueTheme,
                )
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
          itemCount: items.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          controller: controller,
          physics: NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.all(2.0),
          itemBuilder: (context, position) {
            String source = items[position].source.name,
                title = items[position].title,
                body = items[position].description,
                link = items[position].url,
                image = items[position].urlToImage,
                date = items[position].publishedAt;

            return createCard(source, title, body, link, image, date);
          }
        );
    }
  }

  //Method untuk membangun list dan search bar nya
  Widget listViewWidget(List<Articles> article) {
    return ListView(
      shrinkWrap: true,
      controller: controller,
      padding: EdgeInsets.all(10.0),
      children: <Widget>[
        StickyHeader(
          header: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: TextFormField(
              keyboardType: TextInputType.text,
              autofocus: false,
              onChanged: (value) {
                 filterSearchResults(value);
              },
              controller: editingController,
              decoration: InputDecoration(
                  hintText: 'Search article...',
                  contentPadding: EdgeInsets.all(5.0),
                  prefixIcon: Icon(
                    Icons.search,
                    size: 14,
                    color: AppTheme.blueTheme,
                  ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)))),
            ),
          ),
          content: willWeShowTheList(items.length)
        )
      ],
    );
  }

  //Method untuk mengarahkan user ke link artikel di browser
  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  //Method untuk membuat card untuk masing-masing artikel
  Widget createCard(source, title, desc, url, image, publishDate) {
    DateFormat format = DateFormat('yMMMMd'); //Output example: May 1, 2020
    DateTime date = DateTime.parse(publishDate);
    String resultDate = format.format(date);
    Widget whatImage;
    //Jika tidak ada image dari link, tampilkan image default
    if(image == null){
      whatImage = Image.asset('assets/articleImages/blankThumbSide.jpg');
    }else{
      whatImage = Image.network(
        image,
        //Ketika sedang load image, tampilkan gambar default namun beserta loading circle
        loadingBuilder: (BuildContext context, Widget child,
            ImageChunkEvent loadingProgress) {
          if (loadingProgress == null) return child;
          return Center(
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Image.asset('assets/articleImages/blankThumbSide.jpg'),
                CircularProgressIndicator(
                  value: loadingProgress.expectedTotalBytes != null
                      ? loadingProgress.cumulativeBytesLoaded /
                      loadingProgress.expectedTotalBytes
                      : null,
                ),
              ],
            )
          );
        },
      );
    }

    //Buat cardnya
    return GestureDetector(
        onTap: () {
          _launchURL(url);
        },
        child: Padding(
          padding: EdgeInsets.only(bottom: 10.0),
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                    child: whatImage,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(10.0),
                      bottom: Radius.circular(0),
                    )),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        title,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        desc,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      Divider(
                        color: AppTheme.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              source,
                              style: TextStyle(
                                  color: AppTheme.blueTheme, fontSize: 12),
                            ),
                          ),
                          Text(
                            resultDate,
                            style: TextStyle(
                                color: AppTheme.blueTheme, fontSize: 12),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    Row toolbar = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(widget.title),
      ],
    );

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppTheme.notWhite,
        appBar: AppBar(
          title: toolbar,
          backgroundColor: Colors.lightBlue[800],
        ),
        body: RefreshIndicator(
          child: FutureBuilder(
              future: fetchArticle(),
              builder: (context, snapshot) {
                articles = snapshot.data;
                items = snapshot.data;
                return snapshot.data != null
                    ? listViewWidget(snapshot.data)
                    : Center(
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircularProgressIndicator(),
                          SizedBox(
                            height: 20,
                          ),
                          Text('Loading the Articles... Please Wait')
                        ],
                      ));
              }),
          onRefresh: () {
            return fetchArticle();
          },
        ),
      ),
    );
  }

  //Ketika user menekan tombol return
  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(30.0),
            title: Text('Confirm Exit'),
            content: Text('Are you sure you want to exit the app?'),
            actions: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Text(
                      "NO",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(width: 30.0),
                  GestureDetector(
                    onTap: () => SystemNavigator.pop(),
                    child: Text(
                      "YES",
                      style: TextStyle(
                        color: AppTheme.blue,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                    height: 30.0,
                  ),
                ],
              ),
            ],
          ),
        ) ??
        false;
  }
}