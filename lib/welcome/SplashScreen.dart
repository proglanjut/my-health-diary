import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';

import 'package:myhealthdiary/appTheme.dart';
import 'package:myhealthdiary/welcome/OnboardingPage.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => OnBoarding()),
    );
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: AppTheme.notWhite,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Image.asset(
                'assets/images/logo-transparent.png',
                height: 250,
              ),
            ),
            SizedBox(height: 30), //Iki spacing, mbuh gapaham nganggo padding :v
            Loading(
                indicator: BallPulseIndicator(),
                size: 50.0,
                color: Colors.lightBlue[800]),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return SystemNavigator.pop();
  }
}
