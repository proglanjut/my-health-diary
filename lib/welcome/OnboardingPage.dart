import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:myhealthdiary/appTheme.dart';

import 'package:introduction_screen/introduction_screen.dart';

import 'package:myhealthdiary/welcome/WelcomePage.dart';

//Iki bakal halaman intro
class OnBoardingPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Intro onboarding page',
      home: OnBoarding(),
    );
  }
}

class OnBoarding extends StatefulWidget{
  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding>{
  final introKey = GlobalKey<IntroductionScreenState>();

  void _introFinished(context){
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => WelcomePage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 18.0);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700, color: AppTheme.blue),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: AppTheme.notWhite,
      imagePadding: EdgeInsets.zero,
    );

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: IntroductionScreen(
        key: introKey,
        pages: [
          PageViewModel(
            title: "Your Portable Diary",
            body: "My Health Diary is a diary on your smartphone that can help you live a healthy life.",
            image: Image.asset(
              'assets/images/portable-diary-01.png',
            ),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: "Achieve Your Body Goal",
            body: "I can help you maintain or achieve your ideal weight by monitoring your activities and what you have eaten for a day.",
            image: Image.asset(
              'assets/images/achieve-your-body-goals-01.png',
            ),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: "Be Healthy",
            body: "Be healthy by setting some activities that are very beneficial for your body as your daily target. And I will remind you to achieve that everyday.",
            image: Image.asset(
              'assets/images/be-healthy-01.png',
            ),
            decoration: pageDecoration,
          ),
        ],
        onDone: () => _introFinished(context),
        onSkip: () => _introFinished(context), // You can override onSkip callback
        showSkipButton: true,
        skipFlex: 0,
        nextFlex: 0,
        freeze: false,
        skip: const Text('Skip'),
        next: const Icon(Icons.arrow_forward_ios),
        done: const Text('Done', style: TextStyle(fontWeight: FontWeight.w600)),
        dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          color: Color(0xFFBDBDBD),
          activeSize: Size(22.0, 10.0),
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
        ),
      ),
    );
  }
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        contentPadding: EdgeInsets.all(30.0),
        title: Text('Confirm Exit'),
        content: Text('Are you sure you want to exit the app?'),
        actions: <Widget>[
          Row(
            children: <Widget>[
              GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: Text(
                  "NO",
                  style: TextStyle(
                    color: AppTheme.blue,
                  ),
                ),
              ),
              SizedBox(width: 30.0),
              GestureDetector(
                onTap: () => SystemNavigator.pop(),
                child: Text(
                  "YES",
                  style: TextStyle(
                    color: AppTheme.blue,
                  ),
                ),
              ),
              SizedBox(width: 10.0, height: 30.0,),
            ],
          ),
        ],
      ),
    ) ?? false;
  }
}


